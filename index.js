var fs = require('fs'),
    http = require('http'),
    https = require('https'),
    options = {
        key: fs.readFileSync('./ssl/privatekey.pem'),
        cert: fs.readFileSync('./ssl/certificate.pem'),
    },
    sslPort = 9500,
    express = require('express'),
    db = require('./app/db/db'),
    app = express(),
    apiVersion = 'v1',
    userService = require('./app/services/' + apiVersion + '/user_service'),
    yellService = require('./app/services/' + apiVersion + '/yell_service');

app.use(express.static(__dirname + '/public'));
app.use(express.bodyParser());

userService(app).init();
yellService(app).init();

app.listen(3000);
console.log('Started listening at 3000');

// ssl server
//var server = https.createServer(options, app).listen(sslPort, function(){
  //console.log("Express server listening on port " + sslPort);
//});