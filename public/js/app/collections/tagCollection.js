define(["jquery", 'backbone', 'models/tagModel'],
    function($, Backbone, TagModel) {

    return Backbone.Collection.extend({
        model: TagModel,
        url: '/api/v1/yells/tags'
    });

});