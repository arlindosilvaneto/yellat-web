define(["jquery", 'backbone', 'models/yellModel'],
    function($, Backbone, YellModel) {

    return Backbone.Collection.extend({
        model: YellModel,
        url: '/api/v1/yells'
    });

});