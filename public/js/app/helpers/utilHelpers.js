/**
* Created with Skreamr.
* User: aisoft
* Date: 2014-06-05
* Time: 07:03 PM
* To change this template use Tools | Templates.
*/
define( 'handlebars', function(Handlebars) {
    var IF = 'if';

    //#### Example usage:
    //      {{#condition "<value1> <operator> <value2>"}} 
    //			<content>
    //		[ {{else}} <else content> ]
    //		{{/content}}
    //
    //	Assumptions: operators are separated by spaces
    //		(x === '1' || x === '2') is not supported
    //		use
    //		(x === '1') || (x === '2')
    //	 	($scope.key === 'value') - (NOTE: no quotes around $scope.key) $scope.key will pull the value of key from the scope using Utils.get("scope.key", options) 
    Handlebars.registerHelper( 'condition', function ( expression, options ) {
        return callOtherHelper.call( this, IF, [
            Utils.evaluate( expression, this, options ),
            options
        ] );
    } );
    
    // Utility methods
    
    function callOtherHelper( name, args ) {
        var helper = Handlebars.helpers[ name ];

        if ( !helper ) {
            throw "Helper '" + name + "' not found.";
        }

        return helper.apply( this, args );
    }
    
    function parse( str ) {
        // skip corresponding \' and \"
        var stringRegex = /(?:'(?:[^'\\]|\\\')*')|(?:"(?:[^"\\]|\\\")*")/,
            strings = [],
            sectionRegex = /\([^\)\(]*\)/,
            sections = [],
            match,
            parts,
            tree = {},
            refill = function ( text ) {
                text = text.
                replace( /---(\d+)---/, function ( m, p1 ) {
                    return sections[ p1 ];
                } ).
                replace( /%%%(\d+)%%%/g, function ( m, p1 ) {
                    return strings[ p1 ];
                } );
                return text;
            };

        str = str.trim();
        // change " ( a < b ) " to "a < b"
        if ( str.match( /^\([^)(]+\)$/ ) ) {
            str = str.replace( /^\((.*)\)$/, "$1" ).trim();
        }

        while ( ( match = stringRegex.exec( str ) ) !== null ) {
            str = str.replace( stringRegex, ( "%%%" + strings.length + "%%%" ) );
            strings.push( match[ 0 ] );
        }

        // strings have been replaced, so no confusion of '(' or ')' inside strings
        while ( ( match = sectionRegex.exec( str ) ) !== null ) {
            str = str.replace( sectionRegex, ( "---" + sections.length + "---" ) );
            sections.push( match[ 0 ] );
        }

        parts = str.split( /\s+/ );
        if ( parts.length !== 1 && parts.length !== 3 ) {
            throw "invalid number of arguments " + parts;
        }

        // TODO: can we refill what ever is necessary NOT every thing
        tree.arg1 = refill( parts[ 0 ] );
        if ( parts.length > 1 ) {
            tree.arg1 = parse( tree.arg1 );

            tree.operator = parts[ 1 ];
            tree.arg2 = parse( refill( parts[ 2 ] ) );
        }

        return tree;
    }

    function processTree( tree, context, options ) {
        var arg1,
            arg2,
            operator = tree.operator,
            exp;

        if ( !operator ) {
            exp = tree.arg1;

            switch ( exp ) {
            case "true":
            case true:
                return true;

            case "false":
            case false:
                return false;

            case "undefined":
            case undefined:
                return undefined;

            case "null":
            case null:
                return null;

            default:
                // exp will always be of type string
                // either 'str' or 10 or x.y.[5] etc.
                // in above example 'str' and 10 will be considered primitive, string and number respectively
                if ( isPrimitive( exp ) ) {
                    return getPrimitiveValue( exp );
                }
                //Assume $test is a variable that was saved with {{set}} helper
                else if ( typeof exp === "string" && exp.indexOf( "$" ) === 0 ) {
                    return get( exp.substr( 1 ), options );
                } else {
                    return getContextValue( exp, context );
                }
            }
        }

        arg1 = processTree( tree.arg1, context, options );
        arg2 = processTree( tree.arg2, context, options );

        // TODO: work on short circuit
        switch ( operator ) {
        case "||":
            return arg1 || arg2;
        case "&&":
            return arg1 && arg2;
            /* JSHint prehook does not allow ==
             * case "==":
             *  return arg1 == arg2;
             */
        case "===":
            return arg1 === arg2;
            /* JSHint prehook does not allow !=
             * case "!=":
             * return arg1 != arg2;
             */
        case "!==":
            return arg1 !== arg2;
        case "<":
            return arg1 < arg2;
        case "<=":
            return arg1 <= arg2;
        case ">":
            return arg1 > arg2;
        case ">=":
            return arg1 >= arg2;
        case "%":
            return arg1 % arg2;
        default:
            throw "Operator '" + operator + "' is not implemented.";
        }
    }
});