define(['jquery', 'handlebars'], function($, Handlebars) {

    Handlebars.registerHelper('panel', function(options) {
        var outsideDiv = $('<div>'),
            insideDiv = $('<div>'),
            titleDiv = $('<div>'),
            attribute,
            content;

        // process title
        if(this.title) {
            titleDiv.addClass('panel-title');
            titleDiv.html(this.title);

            insideDiv.append(titleDiv);
        }

        // add informed attributes
        for(attribute in options.hash) {
            outsideDiv.attr(attribute, options.hash[attribute]);
        }

        insideDiv.addClass('panel-content');

        content = outsideDiv.
            addClass('app-panel').
            append(insideDiv.append(options.fn(this)))[0].outerHTML;

        // clean up
        outsideDiv.remove();
        insideDiv.remove();
        titleDiv.remove();

        return content;
    });

    Handlebars.registerHelper('yellPanel', function(options) {
        var outsideDiv = $('<div>'),
            insideDiv = $('<div>'),
            attribute,
            content;

        // add informed attributes
        for(attribute in options.hash) {
            outsideDiv.attr(attribute, options.hash[attribute]);
        }

        insideDiv.addClass('yell-panel-content');

        content = outsideDiv.
            addClass('yell-panel').
            append(insideDiv.append(options.fn(this)))[0].outerHTML;

        // clean up
        outsideDiv.remove();
        insideDiv.remove();

        return content;
    });
});