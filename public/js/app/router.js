"use strict";

define(['backbone'], function (Backbone) {

    return Backbone.Router.extend({

        routes: {
            "":             "home",
            "home":         "home",
            "user":         "user",
            "login":        "login",
        },

        home: function () {
            require(["views/homeView"], function (homeView) {
                var view = new homeView();
                view.render();
            });
        },
        
        user: function() {
            require(["views/userProfileView"], function(userView) {
                var view = new userView();
                view.render();
            });
        },
        
        login: function() {
            require(["views/loginView"], function(loginView) {
                var view = new loginView();
                view.render();
            });
        }
    });
});
