/**
* Created with Skreamr.
* User: aisoft
* Date: 2014-06-02
* Time: 07:27 PM
* To change this template use Tools | Templates.
*/
define(['jquery', 'tools'], function($, tools) {
    var TIMEOUT = 10000;
    
    function getPosition(pooling) {
        // Try W3C Geolocation (Preferred)
        if(navigator.geolocation) {

            var successPosition = function(position) {
                position = {lat: position.coords.latitude, lng: position.coords.longitude};
                
                $(document).trigger('location', [position]);
            };

            var failPosition = function(error) {
                $(document).trigger('location', [{errorCode: 001, error: error}]);
            };

            navigator.geolocation.getCurrentPosition(successPosition, failPosition, this.mapOptions);
            
            if(pooling !== false) {
                setTimeout(getPosition, TIMEOUT);
            }
        }
        // Browser doesn't support Geolocation
        else {
            $(document).trigger('location', [{error: 001}]);
        }
    }
    
    return {
        getPosition: getPosition
    }
});