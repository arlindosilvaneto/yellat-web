require(['jquery', 'backbone', 'app/router'], function($, Backbone, Router) {
    "use strict";

    var init = function() {
        window.App = window.App || {};

        window.App.Router = new Router();
        Backbone.history.start();
    };

    $(document).ready(function() {
        init();
    });

});