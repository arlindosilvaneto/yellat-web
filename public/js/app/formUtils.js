define(['jquery', 'tools', 'i18n', 'foundation'], function($, tools, i18n) {

    var clearForm = function(formId) {
        $('#' + formId)[0].reset();
    };

    var clearValidations = function(formId) {
        $('#' + formId + ' [name]').each(function(i, e) {
            $(this).removeClass("border-error");
            $(this).parent().removeClass("error");
            var spanError = $(this).parent().find('span.error');
            spanError.remove();
        });
    };

    var showError = function(field, message, showValidationText) {
        if(showValidationText) {
            var spanError = $('<span>')
                .html(i18n.i("validation." + message))
                .addClass('error');
            field.parent().append(spanError);
            field.parent().addClass('error');
        } else {
            field.addClass("border-error");
        }

        tools.showError( i18n.i("error.415") );
    };

    var processValidations = function(formId, responseData, showValidationText) {
        if(showValidationText === undefined) {
            showValidationText = true;
        }

        if(responseData.status == 415) {
            var form = $('#' + formId);

            for(var fieldId in responseData.responseJSON) {
                var field = $('[name=' + fieldId + ']', form);
                var message = responseData.responseJSON[fieldId].message;

                showError(field, message, showValidationText);
            }
        } else {
            tools.showError( i18n.i("error." + responseData.status) );
        }
    };

    var processLocalValidations = function(formId, errors, showValidationText) {
        if(showValidationText === undefined) {
            showValidationText = true;
        }

        var form = $('#' + formId);

        var counting = 0;
        for(var fieldId in errors) {
            var field = $('[name=' + fieldId + ']', form);

            showError(field, errors[fieldId], showValidationText);
            counting++;
        }

        return counting > 0;
    };

    var serialize = function(formId) {
        var o = {};
        //    var a = this.serializeArray();
        $('#' + formId).find('input[type="hidden"], input[type="text"], input[type="email"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select').each(function() {
            if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                var $parent = $(this).parent();
                var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                if ($chb != null) {
                    if ($chb.prop('checked')) return;
                }
            }
            if (this.name === null || this.name === undefined || this.name === '') return;
            var elemValue = null;
            if ($(this).is('select')) elemValue = $(this).find('option:selected').val();
            else elemValue = this.value;
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(elemValue || '');
            } else {
                o[this.name] = elemValue || '';
            }
        });
        return o;
    };

    return {
        clearForm: clearForm,
        clearValidations: clearValidations,
        processValidations: processValidations,
        processLocalValidations: processLocalValidations,
        serialize: serialize
    };
});