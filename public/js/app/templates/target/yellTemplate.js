define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['yellTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n    <div class=\"row\" id=\"yell-send\">\n        ";
  stack1 = (helper = helpers.panel || (depth0 && depth0.panel),options={hash:{
    'class': ("large-4 large-offset-4 medium-6 medium-offset-3 small-12 columns")
  },inverse:self.noop,fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "panel", options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n    </div>\n";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n            <div class=\"\">\n                <div class=\"yell-content\">\n                    <textarea id=\"yell-data\" placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "yell.dataLabel", options) : helperMissing.call(depth0, "i", "yell.dataLabel", options)))
    + "\"></textarea>\n                    <span id=\"yellCounter\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "yell.counterLabel", options) : helperMissing.call(depth0, "i", "yell.counterLabel", options)))
    + " <span id=\"yellSize\">";
  if (helper = helpers.maxSize) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.maxSize); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span></span>\n                </div>\n\n                <div id=\"yellTagPanel\">\n                    <div id=\"yellTagList\"></div>\n                </div>\n                \n                <div id=\"yellMarkers\">\n                    <img data-marker=\"food\" src=\"/images/markers/food.png\"/>\n                    <img data-marker=\"shopping\" src=\"/images/markers/shopping.png\"/>\n                    <img data-marker=\"sports\" src=\"/images/markers/sports.png\"/>\n                    <img data-marker=\"education\" src=\"/images/markers/education.png\"/>\n                    <img data-marker=\"automotive\" src=\"/images/markers/automotive.png\"/>\n                </div>\n\n                <div id=\"yellButtons\">\n                    <button id=\"doYell\" class=\"button tiny round success\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "yell.doYellBtn", options) : helperMissing.call(depth0, "i", "yell.doYellBtn", options)))
    + "</button>\n                </div>\n            </div>\n        ";
  return buffer;
  }

  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1._id), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n<div id=\"yellsSection\"></div>";
  return buffer;
  });
});