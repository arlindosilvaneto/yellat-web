define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['homeTemplateOld'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\r\n                            <li class=\"divider\"></li>\r\n                            <li class=\"\"><a id=\"broker\" href=\"#/broker\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.brokerLnk", options) : helperMissing.call(depth0, "i", "home.brokerLnk", options)))
    + "</a></li>\r\n                        ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\r\n                            <li class=\"divider\"></li>\r\n                            <li><a href=\"#/login\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.loginLnk", options) : helperMissing.call(depth0, "i", "home.loginLnk", options)))
    + "</a></li>\r\n                        ";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\r\n                            <li class=\"divider\"></li>\r\n                            <li><a href=\"#/profile\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.userProfileLnk", options) : helperMissing.call(depth0, "i", "home.userProfileLnk", options)))
    + "</a></li>\r\n                            <li class=\"divider\"></li>\r\n                            <li><a href=\"#\" class=\"logoutBtn\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.logoutLnk", options) : helperMissing.call(depth0, "i", "home.logoutLnk", options)))
    + " <span class=\"fa fa-sign-out\"></span></a></li>\r\n                        ";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\r\n                        <li><a id=\"broker\" href=\"#/broker\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.brokerLnk", options) : helperMissing.call(depth0, "i", "home.brokerLnk", options)))
    + "</a></li>\r\n                    ";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\r\n                        <li><a href=\"#/login\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.loginLnk", options) : helperMissing.call(depth0, "i", "home.loginLnk", options)))
    + "</a></li>\r\n                    ";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\r\n                        <li><a href=\"#/profile\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.userProfileLnk", options) : helperMissing.call(depth0, "i", "home.userProfileLnk", options)))
    + "</a></li>\r\n                        <li><a href=\"#\" class=\"logoutBtn\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.logoutLnk", options) : helperMissing.call(depth0, "i", "home.logoutLnk", options)))
    + " <span class=\"fa fa-sign-out\"></span></a></li>\r\n                    ";
  return buffer;
  }

  buffer += "<div class=\"top-bar-content\">\r\n    <div class=\"off-canvas-wrap\">\r\n        <div class=\"inner-wrap\">\r\n            <nav class=\"tab-bar\">\r\n                <section class=\"left-small hide-for-large\">\r\n                    <a class=\"left-off-canvas-toggle menu-icon\"><span></span></a>\r\n                </section>\r\n\r\n                <section class=\"right tab-bar-section\">\r\n                    <h1 class=\"title\"><a href=\"/\">TalkLoud <small>DISCUSSION MATTERS</small></a></h1>\r\n                </section>\r\n\r\n                <section class=\"top-bar-section hide-for-medium hide-for-small\">\r\n                    <ul class=\"right\">\r\n                        <li class=\"divider\"></li>\r\n                        <li class=\"active\"><a id=\"home\" href=\"#/home\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.homeLnk", options) : helperMissing.call(depth0, "i", "home.homeLnk", options)))
    + "</a></li>\r\n                        <li class=\"divider\"></li>\r\n                        <li class=\"\"><a id=\"broker\" href=\"#/players\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.playersLnk", options) : helperMissing.call(depth0, "i", "home.playersLnk", options)))
    + "</a></li>\r\n                        ";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1._id), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n                        ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1._id), {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n                    </ul>\r\n                </section>\r\n            </nav>\r\n\r\n            <aside class=\"left-off-canvas-menu hide-for-large\">\r\n                <ul class=\"off-canvas-list\">\r\n                    <li><label>Player SE</label></li>\r\n                    <li class=\"active\"><a id=\"home\" href=\"#/home\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.homeLnk", options) : helperMissing.call(depth0, "i", "home.homeLnk", options)))
    + "</a></li>\r\n                    <li class=\"\"><a id=\"broker\" href=\"#/players\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "home.playersLnk", options) : helperMissing.call(depth0, "i", "home.playersLnk", options)))
    + "</a></li>\r\n                    ";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1._id), {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n                    <li><label>User Options</label></li>\r\n\r\n                    ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1._id), {hash:{},inverse:self.program(11, program11, data),fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n                </ul>\r\n            </aside>\r\n\r\n            <section class=\"main-section\">\r\n                <div id=\"content\">\r\n                    ";
  stack1 = self.invokePartial(partials.editor, 'editor', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n                </div>\r\n\r\n                <div id=\"footer\">\r\n                    <div class=\"row\">\r\n                        <div class=\"small-9 small-centered columns\">\r\n                            <div class=\"row\">\r\n                                <div class=\"section small-4 columns center\">\r\n                                    <h4></h4>\r\n                                </div>\r\n                                <div class=\"section small-4 columns center\">\r\n                                    <h4></h4>\r\n                                </div>\r\n                                <div class=\"section small-4 columns center\">\r\n                                    <h4></h4>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n\r\n            <a class=\"exit-off-canvas\"></a>\r\n        </div>\r\n    </div>\r\n</div>";
  return buffer;
  });
});