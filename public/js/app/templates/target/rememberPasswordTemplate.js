define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['rememberPasswordTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\r\n    <form id=\"rememberPasswordForm\">\r\n        <div class=\"row\">\r\n            <div class=\"small-12 columns\">\r\n                <label for=\"username\" class=\"required\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "password.userLabel", options) : helperMissing.call(depth0, "i", "password.userLabel", options)))
    + ":</label>\r\n                <input placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "password.userLabel", options) : helperMissing.call(depth0, "i", "password.userLabel", options)))
    + "\" type=\"text\" name=\"email\" id=\"email\" class=\"form-control\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"row buttons\">\r\n            <div class=\"small-12 columns\">\r\n                <a class=\"small-12 button tiny radius secondary\" href=\"#\" id=\"resetBtn\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "password.resetBtn", options) : helperMissing.call(depth0, "i", "password.resetBtn", options)))
    + "</a>\r\n            </div>\r\n        </div>\r\n    </form>\r\n";
  return buffer;
  }

  buffer += "<div class=\"medium-4 hide-for-small columns center users-icon\">\r\n    <span class=\"fa fa-users\"></span>\r\n</div>\r\n";
  stack1 = (helper = helpers.panel || (depth0 && depth0.panel),options={hash:{
    'class': ("large-4 medium-6 columns login-panel")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "panel", options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n<div class=\"large-4 medium-2 columns\"></div>";
  return buffer;
  });
});