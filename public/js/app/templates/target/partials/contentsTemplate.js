define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['contentsTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div id=\"otherContainer\">\r\n    <div id=\"loginPanel\" class=\"hide\"></div>\r\n    <div id=\"userPanel\" class=\"hide\"></div>\r\n    <div id=\"yellPanel\" class=\"hide\"></div>\r\n    <div id=\"yellsPanel\" class=\"hide\"></div>\r\n    <div id=\"tagsPanel\" class=\"hide\"></div>\r\n</div>";
  });
});