define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['menuTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n        <ul class=\"button-group round\">\n            <li><a href=\"/#home\" class=\"button tiny success logo-link\">";
  stack1 = (helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "menu.loginWithLabel", options) : helperMissing.call(depth0, "i", "menu.loginWithLabel", options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a></li>\n            <li><a class=\"button tiny\" id=\"openUserProfile\" href=\"/#user\"><i class=\"fa fa-user\"></i></a></li>\n            <li><a class=\"button tiny\" id=\"openSearch\" href=\"#\"><i class=\"fa fa-search\"></i></a></li>\n            <li><a class=\"button tiny\" href=\"#\" id=\"doFindMe\" title=\"find me\"><i class=\"fa fa-map-marker\"></i></a></li>\n            <li><a class=\"button tiny\" id=\"logoutBtn\" href=\"#\"><i class=\"fa fa-sign-out\"></i></a></li>\n        </ul>\n    ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n        <ul class=\"button-group round\">\n            <li><a href=\"/#home\" class=\"button tiny success logo-link\">";
  stack1 = (helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "menu.loginWithLabel", options) : helperMissing.call(depth0, "i", "menu.loginWithLabel", options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a></li>         \n            <li><a class=\"button tiny\" id=\"loginBtn\" href=\"/#login\"><i class=\"fa fa-user\"></i></a></li>\n            <li><a class=\"button tiny\" href=\"#\" id=\"doFindMe\" title=\"find me\"><i class=\"fa fa-map-marker\"></i></a></li>\n        </ul>\n    ";
  return buffer;
  }

  buffer += "<div id=\"menu\">\n    ";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1._id), {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</div>\n\n<div id=\"googleSearchInput\">\n    <input id=\"searchBox\" class=\"controls\" type=\"text\" placeholder=\"Search Box\">\n</div>";
  return buffer;
  });
});