define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['editorTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\r\n        <div class=\"row collapse\">\r\n            <div class=\"small-10 columns\">\r\n                <input type=\"text\" placeholder=\"query\" id=\"contentQuery\" class=\"input-button\">\r\n            </div>\r\n            <div class=\"small-2 columns\">\r\n                <a href=\"#\" class=\"button postfix\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "editor.goBtn", options) : helperMissing.call(depth0, "i", "editor.goBtn", options)))
    + "</a>\r\n            </div>\r\n        </div>\r\n    ";
  return buffer;
  }

  buffer += "<div class=\"row\">\r\n    ";
  stack1 = (helper = helpers.panel || (depth0 && depth0.panel),options={hash:{
    'class': ("medium-6 small-12 small-centered columns")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "panel", options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n    <div id=\"newContentPanel\" class=\"edit-overlay\">\r\n        <div class=\"row\">\r\n            <div id=\"editorPanel\" class=\"large-6 medium-8 small-12 small-centered columns\">\r\n                <input type=\"text\" name=\"content-title\" placeholder=\"title\" class=\"form-control\"/>\r\n                <textarea id=\"content-editor\" cols=\"80\" rows=\"10\"></textarea>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"small-12 columns right\">\r\n                        <a href=\"#\" data-dropdown=\"drop1\" class=\"button tiny round dropdown\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "editor.actionsBtn", options) : helperMissing.call(depth0, "i", "editor.actionsBtn", options)))
    + "</a><br>\r\n                        <ul id=\"drop1\" data-dropdown-content class=\"f-dropdown\">\r\n                            <li><a id=\"editorSaveBtn\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "editor.saveBtn", options) : helperMissing.call(depth0, "i", "editor.saveBtn", options)))
    + "</a></li>\r\n                            <li><a id=\"editorDraftBtn\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "editor.draftBtn", options) : helperMissing.call(depth0, "i", "editor.draftBtn", options)))
    + "</a></li>\r\n                            <li><a id=\"editorCancelBtn\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "editor.cancelBtn", options) : helperMissing.call(depth0, "i", "editor.cancelBtn", options)))
    + "</a></li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";
  return buffer;
  });
});