define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['loginTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\n        <form id=\"loginForm\">\n            <div class=\"row\">\n                <div class=\"small-12 columns\">\n                    <label for=\"email\" class=\"required\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "login.userLabel", options) : helperMissing.call(depth0, "i", "login.userLabel", options)))
    + ":</label>\n                    <input placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "login_userLabel", options) : helperMissing.call(depth0, "i", "login_userLabel", options)))
    + "\" type=\"email\" name=\"email\" id=\"email\" class=\"form-control\"/>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"small-12 columns\">\n                    <label for=\"password\" class=\"required\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "login.passwordLabel", options) : helperMissing.call(depth0, "i", "login.passwordLabel", options)))
    + ":</label>\n                    <input placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "login_passwordLabel", options) : helperMissing.call(depth0, "i", "login_passwordLabel", options)))
    + "\" type=\"password\" name=\"password\" id=\"password\" class=\"form-control\"/>\n                </div>\n            </div>\n            <div class=\"row buttons\">\n                <div class=\"small-12 columns\">\n                    <a class=\"button tiny radius\" href=\"#\" id=\"loginBtn\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "login.loginBtn", options) : helperMissing.call(depth0, "i", "login.loginBtn", options)))
    + "</a>\n                </div>\n            </div>\n            <div class=\"row buttons\">\n                <div class=\"small-12 columns\">\n                    <a class=\"button secondary tiny radius\" id=\"createUserBtn\" href=\"#\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "login.newUserLnk", options) : helperMissing.call(depth0, "i", "login.newUserLnk", options)))
    + "</a>\n                </div>\n            </div>\n            <div class=\"row buttons\">\n                <div class=\"small-8 small-centered columns border-top\">\n                    <a href=\"#\" id=\"fbLoginBtn\" class=\"\"><span class=\"login-button facebook-sprite\"></span></a>\n                    <a href=\"#\" id=\"twLoginBtn\" class=\"disabled\"><span class=\"login-button twitter-sprite\"></span></a>\n                    <a href=\"#\" id=\"ggLoginBtn\" class=\"\"><span class=\"login-button google-sprite\"></span></a>\n                </div>\n            </div>\n            <div class=\"row forgot-div\">\n                <div class=\"small-12 center columns\">\n                    <a href=\"#/reset-password\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "login.forgotPasswordLnk", options) : helperMissing.call(depth0, "i", "login.forgotPasswordLnk", options)))
    + "</a><br/>\n                </div>\n            </div>\n        </form>\n    ";
  return buffer;
  }

  buffer += "<div class=\"row\">\n    ";
  stack1 = (helper = helpers.panel || (depth0 && depth0.panel),options={hash:{
    'class': ("large-4 large-offset-4 medium-6 medium-offset-3 small-12 columns")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "panel", options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n </div>";
  return buffer;
  });
});