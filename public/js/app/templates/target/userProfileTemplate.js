define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['userProfileTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n        <div class=\"row head-row\">\n            <div class=\"small-12 medium-3 columns center\">\n                <img src=\"";
  if (helper = helpers.image) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.image); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\" alt=\"Gravatar Image\" class=\"gravatar th radius\"/>\n                <div class=\"row hide-for-small-only\">\n                    <div class=\"small-12 columns\">\n                        <span class=\"label radius\">";
  if (helper = helpers.imageSource) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.imageSource); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"small-12 medium-9 columns\">\n                <form id=\"userProfileForm\">\n                    <div class=\"row\">\n                        <div class=\"small-12 columns\">\n                            <div class=\"row\">\n                                <div class=\"medium-3 columns\">\n                                    <label class=\"required\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.firstNameLabel", options) : helperMissing.call(depth0, "i", "profile.firstNameLabel", options)))
    + ":</label>\n                                    <input type=\"text\" name=\"firstName\" id=\"firstName\" placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.firstNameLabel", options) : helperMissing.call(depth0, "i", "profile.firstNameLabel", options)))
    + "\" value=\""
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.firstName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n                                </div>\n                                <div class=\"medium-3 columns\">\n                                    <label for=\"lastName\" class=\"control-label\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.lastNameLabel", options) : helperMissing.call(depth0, "i", "profile.lastNameLabel", options)))
    + ":</label>\n                                    <input type=\"text\" class=\"form-control input-sm\" name=\"lastName\" id=\"lastName\" placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.lastNameLabel", options) : helperMissing.call(depth0, "i", "profile.lastNameLabel", options)))
    + "\" value=\""
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.lastName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n                                </div>\n                                <div class=\"medium-6 columns\"></div>\n                            </div>\n\n                            <div class=\"row\">\n                                <div class=\"medium-8 columns\">\n                                    <label for=\"email\" class=\"control-label\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.emailLabel", options) : helperMissing.call(depth0, "i", "profile.emailLabel", options)))
    + ":</label>\n                                    <div class=\"row collapse\">\n                                        <div class=\"small-2 columns\">\n                                            <span class=\"prefix radius\">@</span>\n                                        </div>\n                                        <div class=\"small-10 columns\">\n                                            <input disabled type=\"email\" class=\"form-control input-sm\" name=\"email\" id=\"email\" placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.emailLabel", options) : helperMissing.call(depth0, "i", "profile.emailLabel", options)))
    + "\" value=\""
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"medium-4 columns\"></div>\n                            </div>\n\n                            <div id=\"passwordPanel\" class=\"row\">\n                                <div class=\"medium-3 columns\">\n                                    <label for=\"password\" class=\"control-label\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.passwordLabel", options) : helperMissing.call(depth0, "i", "profile.passwordLabel", options)))
    + ":</label>\n                                    <input type=\"password\" class=\"form-control input-sm\" name=\"password\" id=\"password\" placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.passwordLabel", options) : helperMissing.call(depth0, "i", "profile.passwordLabel", options)))
    + "\" value=\""
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.password)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n                                </div>\n                                <div class=\"medium-3 columns\">\n                                    <label for=\"password2\" class=\"control-label\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.retypePasswordLabel", options) : helperMissing.call(depth0, "i", "profile.retypePasswordLabel", options)))
    + ":</label>\n                                    <input type=\"password\" class=\"form-control input-sm\" name=\"password2\" id=\"password2\" placeholder=\""
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.retypePasswordLabel", options) : helperMissing.call(depth0, "i", "profile.retypePasswordLabel", options)))
    + "\" value=\""
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.password2)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n                                </div>\n                                <div class=\"medium-6 columns\"></div>\n                            </div>\n\n                            <!--div class=\"row\">\n                                <div class=\"hide-for-small-only medium-12 columns\">\n                                    <div class=\"info-label label round secondary\">\n                                        <span class=\"info-label-title\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.fetchTokenLabel", options) : helperMissing.call(depth0, "i", "profile.fetchTokenLabel", options)))
    + ":&nbsp;</span>\n                                        <span></span>\n                                    </div>\n                                </div>\n                            </div-->\n\n                            <div class=\"row border-top\">\n                                <div class=\"small-12 medium-6 columns\">\n                                    <button id=\"saveBtn\" class=\"button tiny radius\">"
    + escapeExpression((helper = helpers.i || (depth0 && depth0.i),options={hash:{},data:data},helper ? helper.call(depth0, "profile.saveBtn", options) : helperMissing.call(depth0, "i", "profile.saveBtn", options)))
    + "</button>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n    ";
  return buffer;
  }

  buffer += "<div class=\"row\">\n    ";
  stack1 = (helper = helpers.panel || (depth0 && depth0.panel),options={hash:{
    'class': ("small-12 columns")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "panel", options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</div>";
  return buffer;
  });
});