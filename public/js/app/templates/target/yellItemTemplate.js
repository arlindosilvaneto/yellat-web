define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['yellItemTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += "\n    <div class=\"row\">\n        <div class=\"small-12 columns yell-name-panel\">\n            @"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.writer)),stack1 == null || stack1 === false ? stack1 : stack1.lastName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ", "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.writer)),stack1 == null || stack1 === false ? stack1 : stack1.firstName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"yell-picture-panel\">\n            <img class=\"th radius yell-user-picture\" src=\"";
  if (helper = helpers.photo) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.photo); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\" alt=\"\"/>\n        </div>\n        <div class=\"small-12 columns yell-text-panel\">\n            ";
  if (helper = helpers.yell) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.yell); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n        </div>\n    </div>\n    <div class=\"yell-date-panel\">\n        <span class=\"yell-date-span\" title=\"";
  if (helper = helpers.created) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.created); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\"></span>\n    </div>    \n    <div class=\"row collapse yell-control-panel hide\">\n        <div class=\"small-12 columns\">\n            <div class=\"row collapse\">\n                <div class=\"small-12 columns yell-tags-panel\">\n                    ";
  if (helper = helpers.tags) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.tags); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n                </div>\n            </div>\n\n            <div class=\"row collapse\">\n                <div class=\"small-12 columns\">\n                    <div class=\"row collapse\">\n                        <div class=\"small-12 columns yell-map-container\">\n                            <img class=\"yell-map-image th\" src=\"\"/>\n                        </div>                \n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.yellPanel) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.yellPanel); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.yellPanel) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });
});