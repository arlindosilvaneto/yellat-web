define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['yellsTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"\" id=\"yellsPanel\">\n    <div class=\"\">\n        <div class=\"row\">\n            <div class=\"small-12 columns yells-filter\">\n\n            </div>\n        </div>\n        <ul class=\"small-block-grid-1 medium-block-grid-2 large-block-grid-4\" id=\"yellsList\">\n            \n        </ul>\n    </div>\n</div>\n\n<div class=\"row yells-loading\">\n    <div class=\"small-12 large-4 large-offset-4 medium-6 medium-offset-3 columns\">\n        <div>\n            <img src=\"/images/loading3.gif\"/>\n        </div>\n    </div>\n</div>";
  });
});