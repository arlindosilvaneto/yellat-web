define(['handlebars'], function (Handlebars) {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tagsTemplate'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"row\" id=\"tagsPanel\">\n    <div class=\"small-12 columns\">\n        <div class=\"row\">\n            <div class=\"small-12 columns panel-left-close\">\n                <span class=\"fa fa-times\" id=\"close\"></span>\n            </div>\n        </div>\n        <canvas id=\"tagsCloud\">\n            <p>Anything in here will be replaced on browsers that support the canvas element</p>\n            <ul class=\"weighted\" style=\"font-size: 50%\" id=\"weightTags\">\n\n            </ul>\n        </canvas>\n    </div>\n</div>\n\n<script type=\"text/javascript\">\n    $('#tagsCloud').attr('width', $('#tagsPanel').width());\n    $('#tagsCloud').attr('height', window.screen.height * 0.8);\n</script>";
  });
});