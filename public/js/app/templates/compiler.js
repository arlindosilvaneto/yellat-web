define(['handlebars'], function(Handlebars) {

    var compileTemplate = function(template, data) {
        var templateCompiled = Handlebars.compile(template);

        return templateCompiled(data);
    };

    return {
        compileTemplate: compileTemplate
    }
});