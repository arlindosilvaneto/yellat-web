define(function() {

    return {
        default: {
            validation: {
                "001": "Preenchimento obrigatório.",
                "002_5": "Tamanho mínimo de 5 caracteres.",
                "003": "Senhas não conferem."
            },

            // error messages
            error: {
                "400": "Faltam dados requeridos para a operação!",
                "401": "Faça o login e tente novamente!",
                "404": "Não encontrato!",
                "409": "Já existem dados com a informação dada",
                "415": "Verifique os campos e tente novamente."
            },
                
                locationError: {
                    "001": "Houve um erro ao tentar obter sua posição"
                },

            // ok messages
            ok: {
                "profile_update": "Perfil atualizado com sucesso"
            },

            // home view
            home: {
                homeLnk: "Inicial",
                playersLnk: "Jogadores",
                newLnk: "Grite!",
                loginLnk: "Entrar",
                userMenuTitle: "Menu do Usuário",
                userProfileLnk: "Meu Perfil",
                logoutLnk: "Sair"
            },

            menu: {
                loginWithLabel: "Bawl <i class='fa fa-map-marker'></i>"
            },

            editor: {
                goBtn: "Vá",
                actionsBtn: "Ações",
                saveBtn: "Salvar",
                draftBtn: "Salvar Rascunho",
                cancelBtn: "Descartar"
            },

            yell: {
                counterLabel: "faltam",
                dataLabel: "Grite aqui!",
                tagsLabel: "tags",
                doYellBtn: "Grite!",
                ignoreTags: ['para', 'onde', 'aonde', 'voce'],
                punctuation: ['.', ',', '!', '?', ":", ";"]
            },

            yells: {
                listenStatus: "Ouvindo..."
            },

            maps: {
                
            },

            // login view
            login: {
                userLabel: "Email",
                passwordLabel: "Senha",
                forgotPasswordLnk: "esqueci a senha",
                newUserLnk: "Criar com seu Email",
                loginBtn: "Entrar"
            },

            // create user view
            newUser: {
                title: "Novo Usuário",
                userLabel: "Email",
                passwordLabel: "Senha",
                password2Label: "Confirmação",
                createBtn: "Criar"
            },

            // remember password
            password: {
                title: "Resetar Senha",
                userLabel: "Email do usuario",
                resetBtn: "Resetar Senha"
            },

            // user profile view
            profile: {
                title: "Meu Perfil",
                firstNameLabel: "Nome",
                lastNameLabel: "Sobrenome",
                emailLabel: "email",
                passwordLabel: "Senha",
                retypePasswordLabel: "Redigitar senha",
                fetchTokenLabel: "App Token",
                saveBtn: "Salvar",
                cancelBtn: "Fechar",

                imageSource: {
                    Google: 'Foto do  Google',
                    Facebook: 'Foto do Facebook',
                    Local: 'Foto do Gravatar'
                }
            }
        }
    }
});