define(["jquery", 'backbone'],
    function($, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/api/v1/yells'
    });

});