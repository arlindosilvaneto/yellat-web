define(["jquery", 'backbone', "underscore", "tools"],
function($, Backbone, _, tools) {
    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/api/v1/users',

        initialize: function() {
            _.bindAll(this, 'login');
        },

        profileImage: function() {
            if(this.isSocialLogin()) {
                return this.get('photoUrl');
            } else {
                return 'http://www.gravatar.com/avatar/' + $.md5(this.get('email')) + '?s=600';
            }
        },

        loginSource: function() {
            if(this.get('social') !== undefined && this.get('social').facebookId !== undefined) {
                return 'Facebook';
            } else if(this.get('social') !== undefined && this.get('social').googleId !== undefined) {
                return 'Google';
            } else {
                return 'Local';
            }
        },

        isSocialLogin: function() {
            return this.get('social') && ( this.get('social').facebookId || this.get('social').googleId );
        },

        login: function() {
            var loginUrl = this.urlRoot + '/login';

            var promise = $.ajax({
                url: loginUrl,
                data: this.toJSON(),
                type: 'json',
                method: 'post'
            });

            return promise;
        },

        loginFacebook: function() {
            var loginUrl = this.urlRoot + '/login-fb';

            var promise = $.ajax({
                url: loginUrl,
                data: this.toJSON(),
                type: 'json',
                method: 'post'
            });

            return promise;
        },

        loginGoogle: function() {
            var loginUrl = this.urlRoot + '/login-gg';

            var promise = $.ajax({
                url: loginUrl,
                data: this.toJSON(),
                type: 'json',
                method: 'post'
            });

            return promise;
        },

        logout: function() {
            this.localDestroy();
        },

        localSave: function() {
            tools.localSave('se.user', this.toJSON());
        },

        localFetch: function() {
            var data = tools.localFetch('se.user');
            if(data && data !== '') {
                this.set(data);

                tools.setHeader({
                    "auth-token": data.token
                });
            }

            return this;
        },

        localDestroy: function() {
            tools.localRemove('se.user');

            tools.setHeader({
                "auth-token": ""
            });
        }
    });
});