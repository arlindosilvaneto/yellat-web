define(['jquery', 'underscore', 'baseView', 'views/createUserView', "formUtils", "tools",
    'models/userModel',
    'i18n',
    "templates/loginTemplate",
    'stickit'],
    function($, _, BaseView, CreateUserView, formUtils, tools, userModel) {

        return BaseView.extend({
            el: '#contents',
            template: Handlebars.templates['loginTemplate'],

            initializeLocal: function() {
                _.bindAll(this, 'loginFacebook', 'loginGoogle');
            },

            render: function() {
                var html = this.template({});

                this.html(html);

                // initialize data binding
                this.stickit();

                return this;
            },
            bindings: {
                "#email": "email",
                "#password": "password"
            },
            events: {
                "click #close": function() {
                    this.hidePanel();
                },
                "keyup #loginForm input": function(event) {
                    this.processLoginKey(event);
                },
                "click #loginBtn": function(event) {
                    event.preventDefault();

                    this.login();
                },
                "click #fbLoginBtn": function(event) {
                    event.preventDefault();

                    FB.login(this.loginFacebook, {scope: 'email'});
                },
                "click #ggLoginBtn": function(event) {
                    event.preventDefault();

                    gapi.auth.authorize({
                        'client_id' : window.social.google.clientId,
                        'cookiepolicy' : 'single_host_origin',

                        'scope': 'https://www.googleapis.com/auth/plus.login email'
                    }, this.loginGoogle);
                },
                "click #createUserBtn": function(event) {
                    event.preventDefault();

                    new CreateUserView().render();
                    this.$('#createUserPanel').fadeIn('slow');
                }
            },
            login: function() {
                var self = this;

                var promise = this.model.login();

                promise.fail(function(err) {
                    formUtils.processValidations('loginForm', err, false);
                });

                promise.done(function(data) {
                    self.model.set(data);
                    self.model.localSave();

                    window.App.Router.navigate('home', {trigger: true});
                });
            },
            loginFacebook: function(response) {
                var self = this;

                if(response.status === 'connected') {
                    FB.api('/me', {fields: 'id,first_name,last_name,email'}, function(me) {
                        var data = {};

                        /*
                         * Process data
                         */
                        data.id = me.id;
                        data.first_name = me.first_name;
                        data.last_name = me.last_name;
                        data.email = me.email;

                        self.user = new userModel(data);
                        var promise = self.user.loginFacebook();

                        promise.fail(function(err) {
                            tools.showError(err);
                        });

                        promise.done(function(data) {
                            self.user.set(data);
                            self.user.localSave();

                            window.App.Router.navigate('home', {trigger: true});
                        });
                    });
                }
            },
            loginGoogle: function(response) {
                var self = this,
                    googleRequest;

                if(! response.error) {
                    gapi.client.load('plus','v1', function(){
                        googleRequest = gapi.client.plus.people.get({
                            'userId': 'me'
                        });

                        googleRequest.execute(function(me) {
                            var data = {};

                            /*
                             * Process data
                             */
                            data.id = me.id;
                            data.first_name = me.name.givenName;
                            data.last_name = me.name.familyName;
                            data.email = me.emails[0].value;
                            data.photo = me.image.url;

                            self.user = new userModel(data);
                            var promise = self.user.loginGoogle();

                            promise.fail(function(err) {
                                tools.showError(err);
                            });

                            promise.done(function(data) {
                                self.user = new userModel(data);
                                self.user.localSave();

                                window.App.Router.navigate('home', {trigger: true});
                            });
                        });
                    });
                }
            },
            processLoginKey: function(e) {
                if(e.which === 13) {// enter key
                    this.login();
                }
            }
        });
    });