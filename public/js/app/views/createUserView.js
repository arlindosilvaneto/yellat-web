define(['jquery', 'underscore', 'baseView', "formUtils", "tools", "compiler",
    'models/userModel',
    'i18n',
    "templates/createUserTemplate",
    'stickit'],
    function($, _, BaseView, formUtils, tools, Compiler, userModel, i18n) {

        return BaseView.extend({
            el: "#loginPanel",
            template: Handlebars.templates['createUserTemplate'],

            initialize: function() {
                this.model = new userModel();
            },

            render: function() {
                var html = this.template();

                this.html(html);

                // initialize data binding
                this.stickit();

                return this;
            },
            bindings: {
                "#email": "email",
                "#password": "password",
                "#password2": "password2"
            },
            events: {
                "click #close": function() {
                    this.hidePanel();
                },
                "keyup #createUserForm input": function(event) {
                    this.processCreateUserKey(event);
                },
                "click #createUserBtn": function(event) {
                    event.preventDefault();

                    formUtils.clearValidations('createUserForm');

                    this.createUser();
                }
            },
            createUser: function() {
                var self = this;

                // local validations
                var errors = {};
                if(this.model.get('password') != this.model.get('password2')) {
                    errors.password = "003";
                } else {
                    delete this.model.attributes.password2;
                }

                var invalid = formUtils.processLocalValidations('createUserForm', errors, false);

                if(! invalid) {
                    var promise = this.model.save();

                    promise.fail(function(err) {
                        formUtils.processValidations('createUserForm', err, false);
                    });

                    promise.done(function() {
                        self.model.localSave();

                        self.navigate("/");
                    });
                }
            },
            processCreateUserKey: function(e) {
                if(e.which === 13) {// enter key
                    this.createUser();
                }
            }
        });
    });