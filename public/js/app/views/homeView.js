define(['jquery', 'baseView','views/mapsView', 'views/yellView', 'views/yellsView',
    "models/userModel",
    'tools',
    "position",
    "i18n",
    "templates/homeTemplate",
    "templates/partials/menuTemplate",
    'templates/partials/contentsTemplate',
    'templates/partials/mapsTemplate',
    "layoutHelpers",
    'foundation',],
    function($, BaseView, MapsView, YellView, YellsView, UserModel, tools, position, i18n) {

        return BaseView.extend({
            el: '.container',
            template: Handlebars.templates['homeTemplate'],
            userProfileView: null,
            yellView: null,
            yellsView: null,
            
            initializeLocal: function() {
                var self = this;
                
                $(document).on('location', function(event, data) {
                    if(data && data.error) {
                        tools.showError(i18n.i("locationError." + data.errorCode));
                    }
                    
                    //console.log(data);
                });
                
                position.getPosition(true);
            },

            render: function() {
                Handlebars.registerPartial('menu', Handlebars.templates['menuTemplate']);
                Handlebars.registerPartial('contents', Handlebars.templates['contentsTemplate']);
                
                var html = this.template({user: this.user.toJSON()});

                this.html(html);
                
                this.yellView = new YellView().render();
                this.yellViews = new YellsView().render();

                this.initFoundationBar();
            },
            initFoundationBar: function() {
                $(document).foundation();
            },  
            events: {
                "click #logoutBtn": function(event) {
                    event.preventDefault();

                    this.user.logout();
                    window.location.reload();
                },

                "click #openSearch": function(event) {
                    event.preventDefault();

                    this.$('#searchBox').toggle('slow');
                },

                "click #doFindMe": function(event) {
                    event.preventDefault();

                    if(this.mapsView) {
                        this.mapsView.locateMe(false);
                    }
                },
            }
        });
    });