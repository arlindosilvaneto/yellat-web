define(['jquery', 'backbone', 'handlebars',
    'models/userModel'], function($, Backbone, Handlebars, userModel) {
    return Backbone.View.extend({
        initialize: function() {
            /*
             * User initialization
             */
            this.user = this.user || new userModel();
            this.user.localFetch();
            
            if(this.initializeLocal) {
                this.initializeLocal();
            }
        },

        navigate: function(url) {
            url = url || "/";

            window.App.Router.navigate(url, {trigger: true});
            //window.location.hash = url;
        },

        html: function(content) {
            this.$el.html(content);

            this.$('#content').hide().fadeIn();
        },

        append: function(content) {
            this.$el.append(content);
        },

        prepend: function(content) {
            this.$el.prepend(content);
        },

        focusFirst: function() {
            this.$("input,select,textarea").
                filter(":not([readonly='readonly']):not([disabled='disabled']):not([type='hidden'])").
                first().
                focus();
        },

        showPanel: function(callback) {
            var self = this;

            //$('#menu').fadeOut('slow');

            this.$el.fadeIn('slow', function() {
                self.focusFirst();

                if(callback) {
                    callback();
                }
            });
        },

        hidePanel: function() {
            this.$el.fadeOut('slow', function() {
                //$('#menu').fadeIn();
            });
        }
    });
});
