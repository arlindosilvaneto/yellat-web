define(['jquery', 'underscore', 'baseView', "formUtils", "tools",
    'collections/yellCollection',
    'views/yellItemView',
    'models/userModel',
    'i18n',
    "templates/yellsTemplate"
    ],
function($, _, BaseView, formUtils, tools, YellCollection, YellItemView, i18n) {

    return BaseView.extend({
        el: "#yellsSection",
        template: Handlebars.templates['yellsTemplate'],
        lastId: null,
        filters: [],
        location: null,
        circleLengthKM: 111,
        maxRadiusKM: .2,
        
        initializeLocal: function() {
            var self = this;
            
            $(document).on('location', function(event, data) {
                if(data && data.error === undefined) {
                    self.location = data;
                    
                    self.processLocation();
                }
            });
        },

        render: function() {
            this.html( this.template() );
            
            return this;
        },

        events: {
            "click #close": function() {
                this.hidePanel();
            }
        },

        renderYells: function(fromLast) {
            var self = this,
                yellList = new YellCollection(),
                data = {
                    lat: this.location.lat,
                    lng: this.location.lng,
                    limit: 10,
                    filter: this.filters
                },
                itemView;

            if(fromLast) {
                data.lastId = this.lastId;
            } else if(this.lastId === undefined) {
                this.$('#yellsList').html('');
            }

            yellList.fetch({
                data: data,
                success: function() {
                    $('.yells-loading').hide();
                    
                    /*
                     * update last id
                     */
                    if(yellList.length > 0) {
                        self.lastId = yellList.at(0).get('yellId');
                    }

                    /*
                     * Process yells
                     */
                    $.each(yellList.models.reverse(), function(index, yell) {
                        itemView = new YellItemView({
                                model: yell
                            }).render(self);

                        self.$('#yellsList').prepend( itemView.el );
                    });
                },
                error: function(err) {
                    console.log(err);
                }
            });
        },
        
        processLocation: function() {
            this.renderYells(true);
        },

        processFilter: function(filter) {
            if($.inArray(filter, this.filters) < 0) {
                this.filters.push(filter);

                this.renderFilters();
            }
        },

        removeFilter: function(tag) {
            var index;

            for(index in this.filters) {
                if(this.filters[index] === tag.attr('data-filter')) {
                    this.filters.splice(index, 1);
                }
            }

            this.renderFilters();
        },

        renderFilters: function() {
            var self = this;

            this.$('.yells-filter').html('');

            $.each(this.filters, function(index, tagName) {
                self.$('.yells-filter').append(tools.createTagLabel(tagName, function(tag) {
                    self.removeFilter(tag);
                }));
            });

            this.renderYells(false);
        }
    });
});