    define(['jquery', 'underscore', 'baseView', "formUtils", "tools", 
    'models/userModel',
    'i18n',
    "templates/yellItemTemplate",
    'stickit',
    'prettyDate',
    'emoticons'],
function($, _, BaseView, formUtils, tools, UserModel, i18n) {

    return BaseView.extend({
        tagName: 'li',
        attributes: {
            
        },
        parentView: null,
        template: Handlebars.templates['yellItemTemplate'],
        googleMapImageUrlTemplate: 'http://maps.googleapis.com/maps/api/staticmap?center=[lat],[lng]&zoom=[zoom]&size=[width]x[height]&maptype=roadmap&markers=icon:[icon]|[lat],[lng]',
        
        initializeLocal: function() {
            _.bindAll(this, 'showOverlay');
        },

        render: function(parentView) {
            this.parentView = parentView;

            var html = this.template(this.processYell());

            this.html(html);

            this.$('.yell-date-span').prettyDate();
            this.$('.yell-text-panel').emoticonize({
                delay: 800,
                animate: true,
                exclude: 'pre, code, .no-emoticons'
            });

            return this;
        },

        events: {
            "click .yell-tag": function(event) {
                var value = this.$el.find(event.currentTarget).attr('data-filter');

                this.parentView.processFilter(value);
            },
            
            "click .yell-user-picture, .yell-panel": 'showOverlay'
        },
        
        showOverlay: function() {
            var self = this,
                localContent = this.$el,
                imageObject = this.$('.yell-map-image'),
                imagePanelContainer = this.$('.yell-map-container'),
                controlPanel = this.$('.yell-control-panel'),
                overlayObject = $('.overlayPanel'),
                closeOverlay = function() {
                    controlPanel.slideUp({complete: function() {
                        overlayObject.removeClass('yell-overlay');
                        localContent.removeClass('yell-overlay-content');
                    }});
                },
                marker = 'default';
            
            if(controlPanel.is(':visible')) {
                closeOverlay();
            } else {
                
                if(this.model.get('marker')) {
                    marker = this.model.get('marker');
                }
                
                // set the complete marker url
                marker = window.location.protocol + '//' + window.location.host + '/images/markers/' + marker + '.png';
            
                overlayObject.addClass('yell-overlay').on('click', function() {
                    closeOverlay();
                });

                localContent.addClass('yell-overlay-content');
                
                // show map image
                controlPanel.slideDown('slow', function() {
                    if(imageObject.attr('src') === '') {
                        imageObject.attr('src',
                                self.googleMapImageUrlTemplate
                                    .replace(/\[lat\]/g, self.model.get('location').lat)
                                    .replace(/\[lng\]/g, self.model.get('location').lng)
                                    .replace(/\[zoom\]/, 16)
                                    .replace(/\[width\]/, imagePanelContainer.width())
                                    .replace(/\[height\]/, parseInt(imagePanelContainer.width()/2, 10))
                                    .replace(/\[icon\]/, marker)
                                );
                    }
                });

                
            }
        },     

        processYell: function() {
            var self = this,
                processedYell = '',
                tags = this.model.get('tags'),
                tagList = '';

            $.each(tags, function(index, tag) {
                tagList += tools.createTagLabel(tag.trim())[0].outerHTML;
            });

            return {
                created: this.model.get('created'),
                photo: this.model.get('writer') ? new UserModel(this.model.get('writer')).profileImage() : '',
                yell: this.model.get('yell'),
                writer: this.model.get('writer'),
                tags: tagList
            };
        }
    });
});