define(['jquery', 'baseView', 'tools'], function($, BaseView, tools) {

    return BaseView.extend({
        mapOptions: {
            enableHighAccuracy: true,
            timeout: 5000
        },
        location: null,
        mapLocation: null,
        map: null,
        myMarker: null,

        el: '#maps-container',

        initializeLocal: function() {
            this.mapsOptions = {
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                mapTypeControl: false,
                streetViewControl: false
            };
            
            $('app').on('location', function(data) {
                if(data && data.error === undefined) {
                    this.location = data;

                    this.processLocation();
                }
            });
        },

        render: function() {
            this.map = this.map || new google.maps.Map(this.$el[0], this.mapsOptions);
        },

        initSearch: function() {
            var input,
                searchBox;

            input = document.getElementById('searchBox');
            this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            searchBox = new google.maps.places.SearchBox(input);

            google.maps.event.addListener(searchBox, 'places_changed', function() {
                console.log(searchBox.getPlaces());
            });
        },
        
        processLocation: function() {
            locateMe();
        },

        locateMe: function(createMark) {
            self.mapLocation = new google.maps.LatLng(this.location.lat, this.location.lng);

            if(createMark !== false) {
                this.markMe();
            }

            this.centerMe();
        },

        centerMe: function() {
            this.map.panTo(this.mapLocation);

            this.myMarker.setPosition(this.mapLocation);
        },

        markMe: function() {
            var self = this;

            this.myMarker = new google.maps.Marker({
                    position: this.mapLocation,
                    draggable: true,
                    animation: google.maps.Animation.DROP
                });

            google.maps.event.addListener(this.myMarker, 'dragend', function(event) {
                self.mapLocation = event.latLng;

                self.centerMe();
                
                $('app').trigger('location', [{lat: event.latLng.lat(), lng: event.latLng.lng()}]);
            });

            google.maps.event.addListener(this.map, 'dblclick', function(event) {
                self.mapLocation = event.latLng;
                self.myMarker.setPosition(self.mapLocation);

                event.stop();
                
                $('app').trigger('location', [{lat: event.latLng.lat(), lng: event.latLng.lng()}]);
            });

            this.myMarker.setMap(this.map);
        }
    });
});