define(['jquery', 'underscore', 'baseView', "formUtils", "tools",
    'models/yellModel',
    'i18n',
    "templates/yellTemplate",
    'stickit'],
    function($, _, BaseView, formUtils, tools, YellModel, i18n) {

        return BaseView.extend({
            el: "#contents",
            template: Handlebars.templates['yellTemplate'],
            yellMaxSize: 50,
            location: null,
            minTagLength: 4,
            
            initializeLocal: function() {
                var self = this;
                
                $(document).on('location', function(event, data) {
                    if(data && data.error === undefined) {
                        self.location = data;
                        
                        self.processLocation();
                    }
                });
            },

            render: function() {
                this.model = new YellModel({
                    tags: [],
                    location: {}
                });

                var html = this.template({user: this.user.toJSON(), maxSize: this.yellMaxSize});

                this.html(html);

                // initialize data binding
                this.stickit();                             

                return this;
            },

            events: {
                "click #close": function() {
                    this.hidePanel();
                },
                "click #doYell": function(event) {
                    var self = this;

                    event.preventDefault();

                    if(this.model.get('yell').trim() === '') {
                        return;
                    } else if (this.model.get('yell').length > this.yellMaxSize) {
                        this.model.set('yell', this.model.get('yell').substr(0, this.yellMaxSize));
                    }

                    this.model.set('location', this.location);

                    this.model.save(null, {
                        success: function() {
                            self.model.clear();
                            self.unmarkAll();
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                },
                "click #yellMarkers > img": function(event) {
                    var clickedMarker = $(event.currentTarget).attr('data-marker'),
                        lastMarked = this.unmarkAll();
                    
                    if(lastMarked !== clickedMarker) {
                        $(event.currentTarget).addClass('th');
                        this.model.set('marker', clickedMarker);
                    } else {
                        this.model.unset('marker');
                    }
                },

                "keyup #yell-data": function(event) {
                    var noMoreThan = this.yellMaxSize - this.model.get('yell').length,
                        tooLong = this.$('#yellCounter').hasClass('yell-too-long');

                    this.$('#yellSize').html(
                        noMoreThan
                    )

                    if(noMoreThan < 0) {
                        this.$('#yellCounter').addClass('yell-too-long');
                    } else if(tooLong) {
                        this.$('#yellCounter').removeClass('yell-too-long');
                    }
                }
            },

            bindings: {
                "#yell-data": "yell"
            },
            
            unmarkAll: function() {
                var lastMarked;
                
                $('#yellMarkers > img').each(function(index, element) {
                    if($(element).hasClass('th')) {
                        lastMarked = $(element).attr('data-marker');
                    }
                    
                    $(element).removeClass('th');
                });
                
                return lastMarked;
            },
            
            processLocation: function() {
                
            },

            createTagLabel: function(tagName) {
                var self = this,
                    tagLabel = $('<span>').
                    addClass('yell-tag').
                    addClass('label').
                    addClass('radius').
                    html(tagName),
                    closeBtn;

                closeBtn = $('<span>').
                    html('  &times;').
                    css('font-size', 'bigger').
                    on('click', function() {
                        var pos = $.inArray(tagName, self.model.get('tags'));

                        self.model.get('tags').splice(pos, 1);

                        tagLabel.remove();
                    });

                tagLabel.append(closeBtn);

                this.$('#yellTagList').append(tagLabel);
            }
        });
    });