define(['jquery', 'underscore', 'baseView', "formUtils", "tools",
    'collections/tagCollection',
    'i18n',
    "templates/tagsTemplate",
    "templates/partials/tagsListTemplate",
    'stickit'],
function($, _, BaseView, formUtils, tools, TagCollection, i18n) {

    return BaseView.extend({
        el: "#tagsPanel",
        template: Handlebars.templates['tagsTemplate'],
        templateList: Handlebars.templates['tagsListTemplate'],
        lastId: null,
        map: null,
        tagList: null,
        tagCanvasInitialized: false,

        render: function(map) {
            var self = this;

            this.map = map;

            this.showPanel(function() {
                self.renderTags(function() {
                    self.initCloud();
                });
            });

            return this;
        },

        events: {
            "click #close": function() {
                this.hidePanel();
            }
        },

        renderTags: function(callback) {
            var self = this,
                bounds;

            this.tagList = new TagCollection();

            bounds = this.map.getBounds();

            this.tagList.fetch({
                data: {
                    swlat: bounds.getSouthWest().lat(),
                    swlng: bounds.getSouthWest().lng(),
                    nelat: bounds.getNorthEast().lat(),
                    nelng: bounds.getNorthEast().lng(),
                    limit: 10
                },
                success: function() {
                    var html,
                        list;

                    if(! self.tagCanvasInitialized) {
                        html = self.template();
                        self.html(html);
                    }

                    list = self.templateList({tags: self.tagList.toJSON()});
                    self.$('#weightTags').html(list);

                    if(callback) {
                        callback();
                    }
                },
                error: function(err) {
                    console.log(err);
                }
            });
        },

        initCloud: function() {
            if(! this.tagCanvasInitialized) {
                var gradient = {
                    17: '#f00', // red
                    0.33: '#ff0', // yellow
                    0.5: 'orange', // orange
                    0.66: '#0f0', // green
                    1: '#00f' // blue
                };

                $('#tagsCloud').tagcanvas({
                    weightGradient: gradient,
                    weight: true,
                    weightFrom: 'data-weight',
                    shadow: '#ccf',
                    shadowBlur: 3,
                    interval: 20,
                    textFont: 'Impact,Arial Black,sans-serif',
                    textColour: '#333333',
                    textHeight: 20,
                    outlineColour: '#F2F0F0',
                    outlineThickness: 5,
                    maxSpeed: 0.05,
                    minBrightness: 0.1,
                    depth: 0.92,
                    pulsateTo: 0.2,
                    pulsateTime: 0.75,
                    initial: [0.1, -0.1],
                    decel: 0.98,
                    reverse: true,
                    hideTags: false
                }, 'weightTags');

                this.tagCanvasInitialized = true;

            } else {
                $('#tagsCloud').tagcanvas('update');
            }
        }
    });
});