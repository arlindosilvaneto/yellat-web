define(['jquery', 'underscore', 'baseView', "formUtils", "tools", "compiler",
    "text!templates/rememberPasswordTemplate.hbs",
    'models/userModel',
    'i18n',
    'stickit'],
    function($, _, BaseView, formUtils, tools, Compiler, rememberPasswordTemplate, userModel, i18n) {

        return BaseView.extend({
            tagName: 'div',
            attributes: {
                class: "row"
            },
            template: rememberPasswordTemplate,

            initialize: function() {
                this.model = new userModel();
            },

            render: function() {
                var html = Compiler.compileTemplate(this.template, {title: i18n.i("password.title")});

                this.$el.html(html);

                $('#content').html(this.$el);

                // initialize data binding
                this.stickit();

                return this;
            },
            bindings: {
                "#email": "email"
            },
            events: {
                "keyup #createUserForm input": function(event) {
                    this.processCreateUserKey(event);
                },
                "click #resetBtn": function(event) {
                    event.preventDefault();

                    formUtils.clearValidations('rememberPasswordForm');

                    this.resetPassword();
                }
            },
            resetPassword: function() {
                var self = this;

                // local validations
                errors = [];
                var invalid = formUtils.processLocalValidations('resetPasswordForm', errors, false);

                if(! invalid) {
                    var promise = this.model.resetPassword();

                    promise.fail(function(err) {
                        formUtils.processValidations('resetPasswordForm', err, false);
                    });

                    promise.done(function() {
                        self.model.localSave();

                        self.navigate("/");
                    });
                }
            },
            processCreateUserKey: function(e) {
                if(e.which === 13) {// enter key
                    this.resetPassword();
                }
            }
        });
    });