define(['jquery', 'baseView',
    'tools',
    'formUtils',
    "i18n",
    "templates/userProfileTemplate",
    'jqueryMd5',
    'stickit'],
function($, BaseView, tools, formUtils, i18n) {

    return BaseView.extend({
        el: '#contents',
        template: Handlebars.templates['userProfileTemplate'],

        render: function() {
            var html = this.template(
                {
                    image: this.user.profileImage(),
                    imageSource: i18n.i("profile.imageSource." + this.user.loginSource()),
                    user: this.user.toJSON()
                });

            this.html(html);

            //this.$el.slideDown('slow');

            this.stickit(this.user);

            //this.showPanel();

            return this;
        },

        bindings: {
            "#firstName": "firstName",
            "#lastName": "lastName",
            "#email": "email",
            "#password": "password",
            "#password2": "password2",
            "#passwordPanel": {
                visible: function() {
                    return ! this.user.isSocialLogin()
                }
            }
        },

        events: {
            "click #close": function() {
                this.hidePanel();
            },
            "click #saveBtn": function(event) {
                var self = this;

                event.preventDefault();

                formUtils.clearValidations('userProfileForm');

                // local validations
                var errors = {};
                if(this.user.get('password') !== '' && this.user.get('password') != this.user.get('password2')) {
                    errors.password = "003";
                } else {
                    delete this.user.attributes.password2;
                }

                var invalid = formUtils.processLocalValidations('userProfileForm', errors);

                if(! invalid) {
                    var promise = this.user.save({wait:true});

                    promise.fail(function(err) {
                        formUtils.processValidations('userProfileForm', err);
                    });

                    promise.done(function(data) {
                        tools.showMessage(i18n.i('ok.profile_update'));

                        self.user.localSave();
                    });
                }
            },

            "click #cancelBtn": function(event) {
                event.preventDefault();

                this.$el.slideUp('slow');
            }
        }
    });
});