module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        shell: {                                // Task
            npm_install: {                      // Target
                options: {                      // Options
                    stdout: true
                },
                command: 'npm install --no-bin-link'
            },

            jasmine_node: {
                options: {
                    stdout: true
                },
                command: 'jasmine-node --forceexit --captureExceptions spec'
            }
        },

        jasmine: {
            webTest: {
                //src: ['public/js/app/**/**.js'],
                options: {
                    specs: 'public/tests/spec/**/**_spec.js',
                    //host: 'http://localhost:3000/',
                    outfile: 'AMD_spec_runner.html',
                    template: require('./node_modules/grunt-template-jasmine-requirejs/src/template-jasmine-requirejs'),
                    templateOptions: {
                        requireConfigFile: './public/js/require-config.js',
                        requireConfig: {
                            baseUrl: './public/js/',
                            waitSeconds: 10
                        }
                    },
                    keepRunner: true
                }
            }
        },

        handlebars: {
            dist: {
                options: {
                    exportAMD: true
                },
                // Glob for a directory of files, builds the files object, then map each
                // one to a new destination file.
                expand: true,
                cwd: 'public/js/app/templates',
                src: '**/*.hbs',
                dest: 'public/js/app/templates/target',
                ext: '.js'
            }
        }
    });

    // Load the plugin that provides tasks.
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-handlebars-compiler');

    // Default task(s).
    grunt.registerTask('default', ['shell:npm_install', 'shell:jasmine_node']);
    grunt.registerTask('test-node', ['shell:jasmine_node']);
    grunt.registerTask('test-web', ['jasmine:webTest']);
    grunt.registerTask('npm', ['shell:npm_install']);
};
