exports.development = {
    db: {
       url: "mongodb://aisoft:super123@kahana.mongohq.com:10044/yell_test"
    },
    baseApi: '/api/v1'
};

exports.test = {
    db: {
       url: "mongodb://aisoft:super123@kahana.mongohq.com:10044/yell_test"
    },
    baseApi: '/api/v1'
};

exports.production = {
    db: {
        url: "mongodb://arlindosilvaneto:super123@oceanic.mongohq.com:10078/sm_prod"
    },
    baseApi: '/api/v1'
};
