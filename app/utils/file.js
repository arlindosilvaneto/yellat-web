var fs = require('fs'),
    request = require('request'),
    Promise = require('mpromise'),
    publicPath = 'public',
    profileImagesPath = '/images/profiles/';

exports.saveProfileImage = function(url, baseFileName, callback) {
    var fileName,
        promise = new Promise();

    request.head({
        url: url,
        followAllRedirects: true
    }, function(err, res){
        //console.log('content-type:', res.headers['content-type']);
        //console.log('content-length:', res.headers['content-length']);

        fileName = profileImagesPath + baseFileName + '.' + res.headers['content-type'].split('/')[1];

        request({
            url: url,
            followAllRedirects: true
        }).pipe(fs.createWriteStream(publicPath + fileName)).on('close', function() {
            //console.log('Image File: ' + fileName);
            promise.fulfill(fileName);
        });
    });

    return promise;
};