/**
 * Description: Player service
 * Author: Arlindo Neto
 * Created at: 2014-01-18
 */

var _ = require('underscore'),
    db = require('../../db/db'),
    async = require('async'),
    env = process.env.NODE_ENV || 'development',
    config = require('../../conf/config')[env],
    auth = require('../auth');

module.exports = function(express) {

    var init = function() {
        express.get(config.baseApi + '/asset', getAssets);
        express.get(config.baseApi + '/asset/stock/:stock', getAssetByStock);
    };

    var params = {};

    var initParams = function(req) {
        params = {
            skip: 0,
            limit: 10,
            query: ''
        };

        _.extend(params, req.query);
    };

    var getAssets = function getAssets(req, res) {
        initParams(req);

        db.init();
        var assetModel = db.assetModel;

        var filter = {};
        if(params.query.trim() != '') {
            var pattern = new RegExp('.*' + params.query + '.*', 'i');
            filter = {stockName: pattern};
        }

        var promise = assetModel.find(filter).skip(params.skip).limit(params.limit).exec();
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(players) {
            res.send(200, players);
        });

        promise.onResolve(function() {

        });
    };

    var getAssetByStock = function getAssetByStock(req, res) {
        var stockName = req.params.stock;

        db.init();
        var assetModel = db.assetModel;

        var promise = assetModel.findOne({stockName: stockName}).exec();
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(player) {
            res.send(200, player);
        });

        promise.onResolve(function() {

        });
    };

    return {
        init: init
    };
}