/**
 * Description: Talk management service
 * Author: Arlindo Neto
 * Created at: 2014-03-10
 */

var _ = require('underscore'),
    db = require('../../db/db'),
    env = process.env.NODE_ENV || 'development',
    config = require('../../conf/config')[env],
    auth = require('../auth');

module.exports = function(express) {

    var init = function() {
        express.get(config.baseApi + '/talks', getTalks);
        express.get(config.baseApi + '/talks/:id', getTalk);
        express.post(config.baseApi + '/talks/', saveTalk);
        express.put(config.baseApi + '/talks/:id', saveTalk);
        express.delete(config.baseApi + '/talks/:id', deleteTalk);
    }

    var getTalks = function getTalks(req, res) {
        var skip, limit,
            promise,
            talk = db.talkModel;

        skip = req.query['skip'] ? req.query['skip'] : 0;
        limit = req.query['limit'] ? req.query['limit'] : 10;

        promise = talk.findTalks(skip, limit);

        promise.onFulfill(function(list) {
            if(list.length == 0) {
                res.send(400);
            } else {
                res.send(200, list);
            }
        });

        promise.onReject(function(err) {
            res.send(500, err);
        });
    };

    var getTalk = function getTalk(req, res) {

    };

    var saveTalk = function saveTalk(req, res) {

    };

    var deleteTalk = function deleteTalk(req, res) {

    };

    return {
        init: init
    };
};