/**
 * Description: Talk management service
 * Author: Arlindo Neto
 * Created at: 2014-03-10
 */

var _ = require('underscore'),
    db = require('../../db/db'),
    env = process.env.NODE_ENV || 'development',
    config = require('../../conf/config')[env],
    auth = require('../auth'),
    tagService = require('../tag');

module.exports = function(express) {

    var init = function() {
        express.get(config.baseApi + '/yells', getYells);
        express.get(config.baseApi + '/yells/tags', getTags);
        express.get(config.baseApi + '/yells/:id', getYell);
        express.post(config.baseApi + '/yells', new auth().checkUser, saveYell);
        express.put(config.baseApi + '/yells/:id', new auth().checkUser, saveYell);
        express.delete(config.baseApi + '/yells/:id', new auth().checkUser, deleteYell);
    }

    var getYells = function getYells(req, res) {
        var lastId, limit,
            swlat, swlng, nelat, nelng,
            filter,
            promise,
            yell = db.yellModel,
            circleReferenceKM = 111,
            maxReferenceKM = .2,
            referenceRatio = maxReferenceKM / circleReferenceKM;

        lastId = req.query['lastId'] ? req.query['lastId'] : undefined;
        limit = req.query['limit'] ? req.query['limit'] : 10;
        filter = req.query['filter'] ? req.query['filter'] : [];

        /*
         * Reference Position
         */
        swlat = req.query['lat'] ? parseFloat(req.query['lat']) - referenceRatio : 0;
        swlng = req.query['lng'] ? parseFloat(req.query['lng']) - referenceRatio : 0;
        nelat = req.query['lat'] ? parseFloat(req.query['lat']) + referenceRatio : 0;
        nelng = req.query['lng'] ? parseFloat(req.query['lng']) + referenceRatio : 0;

        if(swlat === 0 || swlng === 0 || nelat === 0 || nelng === 0) {
            res.send(400, 'No enough data to query');

            return;
        }

        db.init();
        promise = yell.getYells(nelat, nelng, swlat, swlng, filter, limit, lastId);

        promise.onFulfill(function(list) {
            res.send(200, list);
        });

        promise.onReject(function(err) {
            res.send(500, err);
            console.log(err);
        });
    };

    var getTags = function getTags(req, res) {
        var limit,
            swlat, swlng, nelat, nelng,
            promise,
            yell = db.yellModel,
            tagWeights,
            tagList;

        limit = req.query['limit'] ? req.query['limit'] : 100;

        /*
         * Reference Position
         */
        swlat = req.query['lat'] ? parseFloat(req.query['lat']) - referenceRatio : 0;
        swlng = req.query['lng'] ? parseFloat(req.query['lng']) - referenceRatio : 0;
        nelat = req.query['lat'] ? parseFloat(req.query['lat']) + referenceRatio : 0;
        nelng = req.query['lng'] ? parseFloat(req.query['lng']) + referenceRatio : 0;

        if(swlat === 0 || swlng === 0 || nelat === 0 || nelng === 0) {
            res.send(400, 'No enough data to query');

            return;
        }

        db.init();
        promise = yell.getYells(nelat, nelng, swlat, swlng, limit);

        promise.onFulfill(function(list) {
            var maxValue = 1;

            tagWeights = {};
            tagList = [];

            /*
             * Calculate weights
             */
            for(var yellIndex = 0; yellIndex < list.length; yellIndex++) {
                for(var tagIndex = 0; tagIndex < list[yellIndex].tags.length; tagIndex++) {
                    var tag = list[yellIndex].tags[tagIndex],
                        newValue;

                    if(tagWeights[tag]) {
                        newValue = ++tagWeights[tag];

                        if(newValue > maxValue) {
                            maxValue = newValue;
                        }

                        tagWeights[tag] = newValue;
                    } else {
                        tagWeights[tag] = 1;
                    }
                }
            }

            /*
             * Create the result list
             */
            for(var tagName in tagWeights) {
                var weight = tagWeights[tagName],
                    obj = {
                        text: tagName,
                        weight: (weight / maxValue) * 100
                    };

                tagList.push(obj);
            }

            res.send(200, tagList);
        });

        promise.onReject(function(err) {
            res.send(500, err);
        });
    };

    var getYell = function getYell(req, res) {

    };

    var saveYell = function saveYell(req, res) {
        var yell,
            promise;

        yell = new db.yellModel(req.body);

        yell.tags = tagService.extractTags(yell.yell);

        /*
         * Associate the actual user with the yell
         */
        yell.writer = req.user._id;

        db.init();
        promise = yell.createYell();

        promise.onReject(function(err) {
            if(err.name == "ValidationError") {
                res.send(415, err.errors);
            } else {
                res.send(500, err);
            }
        });

        promise.onFulfill(function(savedYell) {
            res.send(201, savedYell);
        });
    };

    var deleteYell = function deleteYell(req, res) {
        var promise,
            id = req.params.id;

        if(id) {
            db.init();
            promise = db.yellModel.findById(id).remove().exec();

            promise.onReject(function(err) {
                res.send(500, err);
            });

            promise.onFulfill(function(data) {
                res.send(200, 'Yell deleted');
            });
        } else {
            res.send(400, 'No enough data to process');
        }
    };

    return {
        init: init
    };
};