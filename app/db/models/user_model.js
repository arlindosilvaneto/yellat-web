var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto'),
    tokenSecret = 'super123';

var schema = new Schema({
    firstName: {type: String, default: '', trim: true},
    lastName: {type: String, default: '', trim: true},
    email: {type: String, default: '', trim: true},
    password:  {type: String, default: '', trim: true},
    creation: {type: Date, default: Date.now},
    active: {type: Boolean, default: true},
    profile: {type: Number, default: 3},
    social: {
        facebookId: {type: String},
        googleId: {type: String},
    },
    photoUrl: {type: String},
    token: {type: String, default: ''},
    fetchToken: {type: String, default: ''}
});

schema.path('firstName').validate(function(value) {
    return !!value;
}, "001");

schema.path('email').validate(function(value) {
    return !!value && /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/.test(value);
}, "001");

schema.path('password').validate(function(value) {
    return !!value;
}, "001");

schema.statics.createPassword = function(data) {
    var md5 = crypto.createHash('md5');
    md5.update(data);

    return md5.digest('hex');
};

schema.statics.findByEmail = function(email) {
    return mongoose.model('User', schema).
        findOne({ email: email }).
        where('social.facebookId').equals(null).
        where('social.googleId').equals(null).
        exec();
};

schema.statics.findByEmailOrName = function(query, skip, limit) {
    var query = mongoose.model('User', schema).
        find().
        or([
            {email: new RegExp(query)},
            {firstName: new RegExp(query.toLowerCase(), 'i')},
            {lastName: new RegExp(query.toLowerCase(), 'i')}
        ]);

    if(skip) {
        query.skip(skip);
    }

    if(limit) {
        query.limit(limit);
    }

    return query.exec();
};

schema.methods.createToken = function(setValue) {
    var token,
        sha1 = crypto.createHash('sha1');

    // user token
    sha1.update(tokenSecret + Date.now());

    token = sha1.digest('hex');
    if(setValue || setValue === undefined) {
        this.token = token;
    }

    return token;
};

schema.methods.createFetchToken = function(setValue) {
    var fetchToken,
        sha1 = crypto.createHash('sha1');

    // user token
    sha1.update(tokenSecret + Date.now());

    fetchToken = sha1.digest('hex');
    if(setValue || setValue === undefined) {
        this.fetchToken = fetchToken;
    }

    return fetchToken;
};

module.exports = schema;
