var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    title: {type: String, default: '', trim: true},
    content: {type: String, default: '', trim: true},
    created: {type: Date, default: Date.now},
    publicAccess: {type: Boolean, default: true},
    tags: [{type: String}],
    parent: {type: Schema.Types.ObjectId, ref: 'Talk'},
    writer: {type: Schema.Types.ObjectId, ref: 'User'}
});

schema.path('title').validate(function(value) {
    return !!value;
}, "001");

schema.path('content').validate(function(value) {
    return !!value;
}, "001");

schema.statics.getTalks = function getTalks(skip, limit) {
    var query = mongoose.model('Talk', schema).findAll().
        sort('writer.firstName').
        sort('writer.lastName').
        sort('-created');

    if(skip) {
        query.skip(skip);
    }

    if(limit) {
        query.limit(limit);
    }

    return query.exec();
};

schema.statics.findByTag = function(tagRegEx, skip, limit) {
    var query = mongoose.model('Talk', schema).
        find().
        where('tags').in([new RegExp(tagRegEx)]).
        sort('writer.firstName').
        sort('writer.lastName').
        sort('-created');

    if(skip) {
        query.skip(skip);
    }

    if(limit) {
        query.limit(limit);
    }

    return query.exec();
};

schema.statics.findByWriter = function(writer, skip, limit) {
    var query = mongoose.model('Talk', schema).
        find({writer: {id: writer._id}}).
        sort('writer.firstName').
        sort('writer.lastName').
        sort('-created');

    if(skip) {
        query.skip(skip);
    }

    if(limit) {
        query.limit(limit);
    }

    return query.exec();
};

module.exports = schema;
