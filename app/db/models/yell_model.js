var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    db = require('../db'),
    Promise = require('mpromise');

var schema = new Schema({
    yellId: {type: Number},
    yell: {type: String, default: '', trim: true},
    created: {type: Date, default: Date.now},
    location: {
        lat: {type: Number, default: 0},
        lng: {type: Number, default: 0}
    },
    tags: [{type: String}],
    writer: {type: Schema.Types.ObjectId, ref: 'User'},
    marker: {type: String, default: ''}
});

schema.path('yell').validate(function(value) {
    return !!value;
}, "001");

schema.statics.getYells = function getYells(NELat, NELng, SWLat, SWLng, tagFilter, limit, lastId) {
    var query = mongoose.model('Yell', schema).find();
    
    query.where('location.lat').gte(SWLat).lte(NELat);
    query.where('location.lng').gte(SWLng).lte(NELng);

    if(tagFilter && tagFilter.length > 0) {
        query.where('tags').in(tagFilter);
    }

    if(limit) {
        query.limit(limit);
    }

    if(lastId) {
        query.where('yellId').gt(lastId);
    }

    query.populate({
        path: 'writer',
        select: 'social photoUrl firstName lastName'
    });

    query.sort('-yellId');

    return query.exec();
};

schema.statics.findByTag = function(tagRegEx, lastId, limit) {
    var query = mongoose.model('Yell', schema).
        find().
        where('tags').in([new RegExp(tagRegEx)]).
        sort('writer.firstName').
        sort('writer.lastName').
        sort('-created');

    if(lastId) {
        query.where('yellId').gt(lastId);
    }

    if(limit) {
        query.limit(limit);
    }

    return query.exec();
};

schema.statics.findByWriter = function(writer, lastId, limit) {
    var query = mongoose.model('Yell', schema).find().
        where({writer: {id: writer._id}});

    if(lastId) {
        query.where('yellId').gt(lastId);
    }

    query.sort('writer.firstName').
        sort('writer.lastName').
        sort('-created');

    if(limit) {
        query.limit(limit);
    }

    return query.exec();
};

schema.methods.createYell = function() {
    var self = this,
        promise = new Promise();

    db.init();

    var counterPromise = db.counterModel.nextYell();

    counterPromise.onReject(function(err) {
        promise.reject(err);
    });

    counterPromise.onFulfill(function(counterData) {
        self.yellId = counterData.yellCounter;

        self.save(function(err, data) {
            if(err) {
                promise.reject(err);
            } else {
                promise.fulfill(data);
            }
        });
    });

    return promise;
}

module.exports = schema;
