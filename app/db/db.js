var mongoose = require('mongoose'),
    env = process.env.NODE_ENV || 'development',
    config = require('../../app/conf/config')[env],

    userSchema = require('./models/user_model'),
    yellSchema = require('./models/yell_model'),
    counterSchema = require('./models/counter_model'),
    talkSchema = require('./models/talk_model'),
    assetSchema = require('./models/asset_model'),
    promise;

exports.init = function(callback) {
    if(! mongoose.connection.readyState) {
        mongoose.connect(config.db.url, {}, function(err) {
            if(err) {
                mongoose.connection.close();
                
                console.log(err);
                throw err;
            }

            if(callback) {
                callback();
            }
        });
    } else if(callback) {
        callback();
    }
};

exports.close = function(callback) {
    mongoose.connection.close(function() {
        if(callback) {
            callback();
        }
    });
};

exports.getDb = function() {
    return mongoose;
};

exports.userModel = mongoose.model('User', userSchema);
exports.yellModel = mongoose.model('Yell', yellSchema);
exports.counterModel = mongoose.model('Counter', counterSchema);
exports.talkModel = mongoose.model('Talk', talkSchema);
exports.assetModel = mongoose.model('Asset', assetSchema);
