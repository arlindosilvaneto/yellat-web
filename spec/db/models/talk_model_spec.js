var db = require('../../../app/db/db');
var helper = require('../../helpers/db-helper')(db);

describe('Talk Model', function() {

    beforeEach(function() {
        db.init();
    });

    it("a talk should have a valid title", function() {
        var talk = new db.talkModel();

        talk.save(function(err) {
            expect(err.errors.title).not.toBeUndefined();
        });
    });

    it("a talk should have a valid content", function() {
        var talk = new db.talkModel();

        talk.save(function(err) {
            expect(err.errors.content).not.toBeUndefined();
        });
    });

    it("should have a related writer", function(done) {
        var user = new db.userModel({
                firstName: 'Arlindo',
                password: 'does not matters',
                email: 'arlindosilvaneto@gmail.com'
            }),
            talk = new db.talkModel({});

        user.save(function(err, savedUser) {
            talk.title = 'test',
            talk.content = 'teste content';
            talk.writer = savedUser;

            talk.save(function(err, savedTalk) {

                expect(savedTalk.writer).toBe(savedUser._id);

                helper.cleanDatabase(done);
            });
        })

    });

    it("should be located by its tags regex", function(done) {
        var promise,
            talk = new db.talkModel({});

        talk.title = 'test';
        talk.content = 'test content';
        talk.tags = ['teste', 'lust'];

        talk.save(function(err, savedTalk) {

            promise = db.talkModel.findByTag('[test]');

            promise.onFulfill(function(list) {
                expect(list.length).toBe(1);
            });

            helper.cleanDatabase(done);
        });

    });

    it("should be a creation date by default", function(done) {
        var talk = new db.talkModel({
            title: 'test',
            content: 'test content'
        });

        talk.save(function(err, savedTalk) {
            expect(savedTalk.created).toBeDefined();

            helper.cleanDatabase(done);
        });

    });

    it("should have public access date by default", function(done) {
        var talk = new db.talkModel({
            title: 'test',
            content: 'test content'
        });

        talk.save(function(err, savedTalk) {
            expect(savedTalk.publicAccess).toBeTruthy();

            helper.cleanDatabase(done);
        });

    });

    it("should be located by its writer", function(done) {
        var promise,
            user = new db.userModel({
                firstName: 'Arlindo',
                password: 'does not matters',
                email: 'arlindosilvaneto@gmail.com'
            }),
            talk = new db.talkModel({});

        user.save(function(err, savedUser) {
            talk.title = 'test',
            talk.content = 'teste content';
            talk.writer = savedUser;

            talk.save(function(err, savedTalk) {

                promise = db.talkModel.findByWriter(savedUser);

                promise.onFulfill(function(list) {
                    expect(savedTalk.title).toBe('test');
                });

                helper.cleanDatabase(done);
            });
        });

    });
});