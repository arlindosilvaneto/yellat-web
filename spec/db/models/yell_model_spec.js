var db = require('../../../app/db/db');
var helper = require('../../helpers/db-helper')(db);

describe('Yell Model', function() {

    beforeEach(function() {
        db.init();
    });

    it("a talk should have a valid content", function() {
        var yell = new db.yellModel();

        yell.save(function(err) {
            expect(err.errors.yell).toBeDefined();
        });
    });

    it("should have a related writer", function(done) {
        var user = new db.userModel({
                firstName: 'Arlindo',
                password: 'does not matters',
                email: 'arlindosilvaneto@gmail.com'
            }),
            yell = new db.yellModel();

        user.save(function(err, savedUser) {
            yell.yell = 'teste yell';
            yell.writer = savedUser;

            yell.save(function(err, savedYell) {

                expect(savedYell.writer).toBe(savedUser._id);

                helper.cleanDatabase(done);
            });
        })

    });

    it("should have the initial id equals 1", function(done) {
        var yell = new db.yellModel({}),
            promise;

        yell.yell = 'test content';
        yell.tags = ['teste', 'lust'];

        promise = yell.createYell();

        promise.onFulfill(function(savedYell) {

            expect(savedYell.yellId).toBe(1);

            helper.cleanDatabase(done);
        });

        promise.onReject(function(err) {
            console.log(err);
        });
    });

    it("should have a default created date", function(done) {
        var yell = new db.yellModel({});

        yell.yell = 'test content';
        yell.tags = ['teste', 'lust'];

        yell.save(function(err, savedYell) {

            expect(savedYell.created).toBeDefined();

            helper.cleanDatabase(done);
        });

    });

    it("should have a default location", function(done) {
        var yell = new db.yellModel({});

        yell.yell = 'test content';
        yell.tags = ['teste', 'lust'];

        yell.save(function(err, savedYell) {

            expect(savedYell.location.lat).toBe(0);
            expect(savedYell.location.lng).toBe(0);

            helper.cleanDatabase(done);
        });

    });

    it("should locate all yells", function(done) {
        var promise,
            yell = new db.yellModel({});

        yell.yell = 'test content';
        yell.tags = ['teste', 'lust'];

        yell.save(function(err, savedYell) {

            promise = db.yellModel.getYells();

            promise.onFulfill(function(list) {
                expect(list.length).toBe(1);
            });

            helper.cleanDatabase(done);
        });

    });

    it("should be located by its tags regex", function(done) {
        var promise,
            yell = new db.yellModel({});

        yell.yell = 'test content';
        yell.tags = ['teste', 'lust'];

        yell.save(function(err, savedYell) {

            promise = db.yellModel.findByTag('[test]');

            promise.onFulfill(function(list) {
                expect(list.length).toBe(1);
            });

            helper.cleanDatabase(done);
        });

    });

    it("should be a creation date by default", function(done) {
        var yell = new db.yellModel({});

        yell.yell = 'test content';
        yell.tags = ['teste', 'lust'];

        yell.save(function(err, savedYell) {
            expect(savedYell.created).toBeDefined();

            helper.cleanDatabase(done);
        });

    });

    it("should be located by its writer", function(done) {
        var promise,
            user = new db.userModel({
                firstName: 'Arlindo',
                password: 'does not matters',
                email: 'arlindosilvaneto@gmail.com'
            }),
            yell = new db.yellModel({});

        user.save(function(err, savedUser) {
            yell.yell = 'teste content';
            yell.writer = savedUser;

            yell.save(function(err, savedYell) {

                promise = db.yellModel.findByWriter(savedUser);

                promise.onFulfill(function(list) {
                    expect(list.length).toBe(1);
                });

                helper.cleanDatabase(done);
            });
        });

    });
});