var db = require('../../../app/db/db');
var helper = require('../../helpers/db-helper')(db);

describe('User Model', function() {

    beforeEach(function() {
        db.init();
    });

    it('should have a valid first name', function(done) {
        var user = db.userModel();

        user.save(function(err) {
            expect(err.errors.firstName).not.toBeUndefined();

            helper.cleanDatabase(done);
        });
    });

    it('should have a valid email', function(done) {
        var user = db.userModel({email: 'arlindo@gmail'});

        user.save(function(err) {
            expect(err.errors.email).not.toBeUndefined();

            helper.cleanDatabase(done);
        });
    });

    it('should have a valid password', function(done) {
        var user = db.userModel();

        user.save(function(err) {
            expect(err.errors.password).not.toBeUndefined();

            helper.cleanDatabase(done);
        });
    });

    it('should be defined with profile 3 (user) by default', function() {
        var user = db.userModel();

        expect(user.profile).toBe(3);
    });

    it('should not consider social network users when logging in with local profiles', function(done) {
        var user = db.userModel,
            email = 'arlindo@aisoft.com.br',
            socialUser = db.userModel({
                firstName: 'Arlindo',
                lastName: 'Social',
                password: 'does not matters',
                email: email,
                social: {
                    facebookId: 'adsfadsf', // for facebook authenticated user
                    googleId: 'asdfadsfd' // for google authenticated user
                }
            }),
            localUser = db.userModel({
                firstName: 'Arlindo',
                lastName: 'Local',
                password: db.userModel.createPassword('password'),
                email: email
            });

        socialUser.save(function(err) {
            localUser.save(function(err) {
                var promise = user.findByEmail(email);

                promise.onFulfill(function(userFound) {
                    expect(userFound.lastName).toBe('Local');

                    helper.cleanDatabase(done);
                });
            });
        });
    });

    it('should locate users by first name partial', function(done) {
        var promise,
            user = db.userModel({
                firstName: 'Arlindo',
                lastName: 'Neto',
                password: 'does not matters',
                email: 'joselito@gmail.com'
            });

        user.save(function(err) {
            promise = db.userModel.findByEmailOrName('lindo');

            promise.onFulfill(function(users) {
                expect(users.length).toBe(1);

                helper.cleanDatabase(done);
            });
        });
    });

    it('should locate users by last name partial', function(done) {
        var promise,
            user = db.userModel({
                firstName: 'Arlindo',
                lastName: 'Neto',
                password: 'does not matters',
                email: 'arlindosilvaneto@gmail.com'
            });

        user.save(function(err) {
            promise = db.userModel.findByEmailOrName('silva');

            promise.onFulfill(function(users) {
                expect(users.length).toBe(1);

                helper.cleanDatabase(done);
            });
        });
    });

    it('should locate users by email partial', function(done) {
        var promise,
            user = db.userModel({
                firstName: 'Arlindo',
                lastName: 'Neto',
                password: 'does not matters',
                email: 'arlindosilvaneto@gmail.com'
            });

        user.save(function(err) {
            promise = db.userModel.findByEmailOrName('eto');

            promise.onFulfill(function(users) {
                expect(users.length).toBe(1);

                helper.cleanDatabase(done);
            });
        });
    });

    it('should locate users by email partial skipping some contents', function(done) {
        var promise,
            user = db.userModel({
                firstName: 'Arlindo',
                lastName: 'Neto',
                password: 'does not matters',
                email: 'arlindosilvaneto@gmail.com'
            });

        user.save(function(err) {
            promise = db.userModel.findByEmailOrName('silva', 1);

            promise.onFulfill(function(users) {
                expect(users.length).toBe(0);

                helper.cleanDatabase(done);
            });
        });
    });

    it('should locate users by email partial with a limit', function(done) {
        var promise,
            user = db.userModel({
                firstName: 'Arlindo',
                lastName: 'Neto',
                password: 'does not matters',
                email: 'arlindosilvaneto@gmail.com'
            });

        user.save(function(err) {
            promise = db.userModel.findByEmailOrName('silva', 0, 1);

            promise.onFulfill(function(users) {
                expect(users.length).toBe(1);

                helper.cleanDatabase(done);
            });
        });
    });
});