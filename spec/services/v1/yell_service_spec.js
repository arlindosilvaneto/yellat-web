var _ = require('underscore'),
    yellService = require('../../../app/services/v1/yell_service'),
    config = ('../../../app/conf/config')[process.env.NODE_ENV],
    db = require('../../../app/db/db'),
    dbHelper = require('../../helpers/db-helper')(db),
    userModel;

describe('Yell Service', function() {
    var fakeExpress = {
            getYells: null,
            getTags: null,
            getYell: null,
            createYell: null,
            updateYell: null,
            deleteYell: null,
            get: function(path, callback) {
                if(callback.name == 'getYells') {
                    this.getYells = callback;
                } else if(callback.name == 'getYell') {
                    this.getYell = callback;
                } else if(callback.name == 'getTags') {
                    this.getTags = callback;
                }
            },
            post: function(path, authModule, callback) {
                if(authModule) {
                    if(callback.name == 'saveYell') {
                        this.createYell = callback;
                    }
                }
            },
            put: function(path, authModule, callback) {
                if(authModule) {
                    if(callback.name == 'saveYell') {
                        this.updateYell = callback;
                    }
                }
            },
            delete: function(path, authModule, callback) {
                if(authModule) {
                    if(callback.name == 'deleteYell') {
                        this.deleteYell = callback;
                    }
                }
            }
        },
        fakeRequest = {
            body: {},
            query: {}
        },
        fakeResponse = {
            status: 0,
            body: '',
            send: function(status, body) {
                if(body === undefined)
                {
                    this.status = 200;
                    this.body = status;
                } else {
                    this.status = status;
                    this.body = body;
                }
            }
        },
        responseSpy = null;

    beforeEach(function(done) {
        fakeRequest = {
            body: {},
            headers: {},
            params: {}
        };
        fakeResponse.status = 0;
        fakeResponse.body = '';

        responseSpy = spyOn(fakeResponse, 'send').andCallThrough();

        db.init();

        // create user for login token
        userModel = db.userModel({
            firstName: "Arlindo",
            email: "arlindosilvaneto@gmail.com",
            password: "does not matters"
        });

        userModel.createToken();
        
        userModel.save(function(err, savedUser) {
            fakeRequest.user = savedUser;
            
            done();
        });
    });

    afterEach(function(done) {
        dbHelper.cleanDatabase(done);
    });

    it('should create yells', function() {
        yellService(fakeExpress).init();
        fakeRequest.body = {
            yell: 'teste yell',
            tags: ['teste', 'tags']
        };

        runs(function() {
            fakeExpress.createYell(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(201);
        });
    });

    it('should delete yells', function() {
        yellService(fakeExpress).init();
        fakeRequest.body = {
            yell: 'teste yell',
            tags: ['teste', 'tags']
        };

        runs(function() {
            fakeExpress.createYell(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            fakeRequest.params.id = fakeResponse.body.id;
            fakeExpress.deleteYell(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
        });
    });

    it('should require id paramter to delete yells', function() {
        yellService(fakeExpress).init();
        fakeRequest.body = {
            yell: 'teste yell',
            tags: ['teste', 'tags']
        };

        runs(function() {
            fakeExpress.createYell(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            fakeExpress.deleteYell(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(400);
        });
    });

    it('should find yells around the given location', function() {
        
        yellService(fakeExpress).init();
        fakeRequest.body = {
            yell: 'teste yell',
            location: {
                lat: 0,
                lng: 0
            }
        };

        runs(function() {
            fakeExpress.createYell(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            fakeRequest.query = {
                lat: 0,
                lng: 0
            };

            fakeExpress.getYells(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        });

        runs(function() {
            expect(fakeResponse.body[0].yell).toBe('teste yell');
        });
    });

    it('should find yells tags inside the given bounds with weight for each one', function() {

        yellService(fakeExpress).init();
        fakeRequest.body = {
            yell: 'tests yells',
            location: {
                lat: 0,
                lng: 0
            }
        };

        runs(function() {
            fakeExpress.createYell(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            fakeRequest.query = {
                lat: 0,
                lng: 0
            };

            fakeExpress.getTags(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        });

        runs(function() {
            expect(fakeResponse.body.length).toBe(2);
        });
    });
});