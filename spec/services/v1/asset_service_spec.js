var _ = require('underscore'),
    assetService = require('../../../app/services/v1/asset_service'),
    config = ('../../../app/conf/config')[process.env.NODE_ENV],
    db = require('../../../app/db/db'),
    dbHelper = require('../../helpers/db-helper')(db);

describe('Asset Service', function() {
    var fakeExpress = {
            getAssets: null,
            getAssetByStock: null,
            get: function(path, callback) {
                if(callback.name == 'getAssets') {
                    this.getAssets = callback;
                } else if(callback.name == 'getAssetByStock') {
                    this.getAssetByStock = callback;
                }
            }
        },
        fakeRequest = {
            body: {},
            query: {}
        },
        fakeResponse = {
            status: 0,
            body: '',
            send: function(status, body) {
                if(body === undefined)
                {
                    this.status = 200;
                    this.body = status;
                } else {
                    this.status = status;
                    this.body = body;
                }
            }
        },
        responseSpy = null;

    beforeEach(function() {
        fakeRequest = {
            body: {},
            query: {}
        };
        fakeResponse.status = 0;
        fakeResponse.body = '';

        responseSpy = spyOn(fakeResponse, 'send').andCallThrough();

        db.init();
    });

    afterEach(function(done) {
        dbHelper.cleanDatabase(done);
    });

    it('List assets should return OK', function() {
        assetService(fakeExpress).init();

        runs(function() {
            fakeExpress.getAssets(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
        });
    });

    it('List assets should return the value inside the informed range', function() {
        assetService(fakeExpress).init();

        var asset = db.assetModel({
            stockName: "ABCD"
        });

        var fail = false;

        runs(function() {
            asset.save(function(err, asset) {

                if(err) {
                    console.log(err);
                    fail = true;
                } else {

                    assetService(fakeExpress).init();
                    fakeRequest.query = {

                    };

                    fakeExpress.getAssets(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body[0].stockName).toBe("ABCD");
        });
    });

    it('List assets should not return the value outside the informed range', function() {
        assetService(fakeExpress).init();

        var asset = db.assetModel({
            stockName: "ABCD"
        });

        var fail = false;

        runs(function() {
            asset.save(function(err, asset) {

                if(err) {
                    console.log(err);
                    fail = true;
                } else {

                    assetService(fakeExpress).init();
                    fakeRequest.query = {
                        skip: 1
                    };

                    fakeExpress.getAssets(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.length).toBe(0);
        });
    });

    it('List assets should return the value that matches the query', function() {
        assetService(fakeExpress).init();

        var asset = db.assetModel({
            stockName: "ABCD"
        });

        var fail = false;

        runs(function() {
            asset.save(function(err, asset) {

                if(err) {
                    console.log(err);
                    fail = true;
                } else {

                    assetService(fakeExpress).init();
                    fakeRequest.query = {
                        query: 'bc'
                    };

                    fakeExpress.getAssets(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body[0].stockName).toBe('ABCD');
        });
    });
});