var _ = require('underscore'),
    talkService = require('../../../app/services/v1/talk_service'),
    config = ('../../../app/conf/config')[process.env.NODE_ENV],
    db = require('../../../app/db/db'),
    dbHelper = require('../../helpers/db-helper')(db);

describe('Talk Service', function() {
    var fakeExpress = {
            getTalks: null,
            getTalk: null,
            saveTalk: null,
            deleteTalk: null,
            get: function(path, callback) {
                if(callback.name == 'getTalks') {
                    this.getTalks = callback;
                } else if(callback.name == 'getTalk') {
                    this.getTalk = callback;
                }
            },
            post: function(path, callback) {
                if(callback.name == 'saveTalk') {
                    this.saveTalk = callback;
                }
            },
            put: function(path, callback) {
                if(callback.name == 'saveTalk') {
                    this.updateUser = callback;
                }
            },
            delete: function(path, callback) {
                if(callback.name == 'deleteTalk') {
                    this.deleteTalk = callback;
                }
            }
        },
        fakeRequest = {
            body: {}
        },
        fakeResponse = {
            status: 0,
            body: '',
            send: function(status, body) {
                if(body === undefined)
                {
                    this.status = 200;
                    this.body = status;
                } else {
                    this.status = status;
                    this.body = body;
                }
            }
        },
        responseSpy = null;

    beforeEach(function() {
        fakeRequest = {
            body: {}
        };
        fakeResponse.status = 0;
        fakeResponse.body = '';

        responseSpy = spyOn(fakeResponse, 'send').andCallThrough();

        db.init();
    });

    afterEach(function(done) {
        dbHelper.cleanDatabase(done);
    });


});