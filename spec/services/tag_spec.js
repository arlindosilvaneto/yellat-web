var tagService = require('../../app/services/tag');

describe('Tag Service', function() {
    it('should convert just some words to tags', function() {
        var text = 'cinco tres seisletras',
            converted = tagService.extractTags(text);

        expect(converted.length).toBe(2);
        expect(converted[0]).toBe('cinco');
        expect(converted[1]).toBe('seisletras');
    });
});